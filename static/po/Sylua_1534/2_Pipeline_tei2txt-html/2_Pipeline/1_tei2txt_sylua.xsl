<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <xsl:output encoding="UTF-8" method="text" indent="no"/>
    
    <xsl:template match="fw"/>
    <xsl:template match="hi[@type='Title']"/>
    
        <xsl:template match="/">
            <xsl:for-each select="//reg[@type = 'ed' and ancestor::div2[@xml:id='Sylua_poem'] and not(ancestor::hi[@rend='Title']) and not(ancestor::fw)]">
                <xsl:choose>
                    <xsl:when test="ancestor::hi[@rend = 'DropCapital']">
                        <xsl:text>&#xA;</xsl:text>
                        <xsl:value-of select="normalize-space(.)"/>
                    </xsl:when>
                    <xsl:when test="contains(., '¬')">
                        <xsl:value-of select="translate(normalize-space(.), '¬', '')"/>
                    </xsl:when>
                    <xsl:when test="contains(., 'non ulla lege')">
                        <xsl:value-of select="normalize-space(.)"/><xml:text> </xml:text>
                    </xsl:when>
                    <xsl:when test="contains(., 'plus minus')">
                        <xsl:value-of select="normalize-space(.)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(.)"/>
                        <xsl:text>&#xA;</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:template>
        
    
   
</xsl:stylesheet>
