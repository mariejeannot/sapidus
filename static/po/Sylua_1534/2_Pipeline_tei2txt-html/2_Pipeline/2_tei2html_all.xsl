<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        exclude-result-prefixes="xs"
        version="2.0"
        xpath-default-namespace="http://www.tei-c.org/ns/1.0">
        
        <xsl:output method="html" encoding="UTF-8"/>
        <xsl:strip-space elements="*"/>
        
        <!-- Je modifie l'élément racine pour construire une page html -->
        
        <xsl:template match="/">
            <html>
                <head>
                    <title>Édition numérique de l'imprimé comprenant la <span rend="italic">Sylua epistolaris seu Barba</span> de Ioannes Sapidus.</title>
                   </head>
                <body>
                    <style>
                        .junicode-text {
                        font-family: 'Junicode', serif;
                        }
                        
                        .verse {
                        display: flex;
                        justify-content: space-between;
                        }
                        
                        .verse-text {
                        margin-left: 20%;
                        }
                        
                        .verse-number {
                        margin-right: 10%;
                        }
                 
                        .hover-note {
                        display: none;
                        position: absolute;
                        background-color: #fff;
                        border: 1px solid #ccc;
                        padding: 10px;
                        width: 200px;
                        font-size: 14px;
                        line-height: 1.5;
                        height: auto;
                        }
                        
                        sup:hover .hover-note {
                        display: inline-block;
                        }
                        
                        .Drop-capital {
                        display: inline;
                        font-weight: 800;
                        font-size: 24px;
                        }
                        
                        .tooltip {
                        position: relative;
                        display: inline-block;
                        color: blue;
                        font-weight: bold;
                        opacity: 1;
                        font-size: 14px;
                        }
                        
                        .tooltip .tooltip-content {
                        visibility: hidden;
                        width: 250px;
                        background-color: #f9f9f9;
                        color: #333;
                        text-align: center;
                        border: 1px solid #ccc;
                        border-radius: 4px;
                        padding: 5px;
                        position: absolute;
                        z-index: 9999;
                        bottom: 125%;
                        left: 50%;
                        transform: translateX(-50%);
                        opacity: 0;
                        transition: opacity 0.3s;
                        }
                        
                        .tooltip:hover .tooltip-content {
                        visibility: visible;
                        opacity: 1;
                        }
                    </style>
                    <xsl:apply-templates/>
                    <xsl:apply-templates select="//app"/>
                    <div id="footnotes">
                        <h2>Notes</h2>
                        <xsl:apply-templates select="//note" mode="footnote"/>
                    </div>
                </body>
            </html>
        </xsl:template>
        
        
        <!-- 
                    <div id="viewer" style="width:50%; height:100%; background-color: #000"></div>
                    <script src="https://unpkg.com/mirador@latest/dist/mirador.min.js"></script>
                    <script type="text/javascript">var miradorInstance = mirador.createMiradorViewer({"manifests":{"https://opendata2.uni-halle.de//json/iiif/1516514412012/177/8563d9a7-d66e-430f-89c6-438587964679/manifest":{"provider":"ULB Sachsen-Anhalt, Halle"}},"miradorDownloadPlugin":{"restrictDownloadOnSizeDefinition":true},"language":"fr","id":"viewer","window":{"textOverlay":{"visible":false,"selectable":true,"enabled":true}},"windows":[{"canvasId":"https://opendata2.uni-halle.de//json/iiif/1516514412012/177/8563d9a7-d66e-430f-89c6-438587964679/manifest/canvas/c0","thumbnailNavigationPosition":"far-bottom","loadedManifest":"https://opendata2.uni-halle.de//json/iiif/1516514412012/177/8563d9a7-d66e-430f-89c6-438587964679/manifest"}],"selectedTheme":"dark"});</script>
                    <div id="sidebar" class="sidebar"></div>
        -->
        
        <!-- Je ne copie pas le contenu de teiHeader and co -->
        
        <xsl:template match="teiHeader"/>
        <xsl:template match="orig"/>
        <xsl:template match="sourceDoc"/>
        <xsl:template match="lb"/>
        <xsl:template match="app"/>
        <xsl:template match="back"/>
        <xsl:template match="figure"/>
        <xsl:template match="reg[@type='norm']"/>
        <xsl:template match="reg[@type='scansion_readable']"/>
        <xsl:template match="reg[@type='scansion_xml']"/>
        
       
        <xsl:template match="anchor"> <!-- attention, il faut modifier le html à la main pour lui faire comprendre que &lt; ouvre une balise, passage obligatoire, car sinon n'aurait pas voulu ouvrir une balise sans la fermer dans le <xsl:when> -->
                <xsl:choose>
                    
                    <xsl:when test="ends-with(@xml:id, 's')">
                        <xsl:text>&lt;span class="tooltip"></xsl:text>
                            <xsl:apply-templates/>
                    </xsl:when>
                   
                    <xsl:when test="ends-with(@xml:id, 'e')">
                        <xsl:text>&lt;span class="tooltip-content"></xsl:text>
                        <xsl:variable name="lem" select="//app[@to=concat('#', current()/@xml:id)]/lem"/>
                        <xsl:variable name="rdg" select="//app[@to=concat('#', current()/@xml:id)]/rdg"/>
                        <xsl:variable name="wit2" select="//app[@to=concat('#', current()/@xml:id)]/rdg/@wit"/>
                        <xsl:variable name="wit1" select="//app[@to=concat('#', current()/@xml:id)]/lem/@wit"/>
                        <xsl:apply-templates select="$lem"/>
                        <xsl:if test="$wit1">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="translate($wit1, '#', '')"/>
                            <xsl:text></xsl:text>
                        </xsl:if>
                        <xsl:text> : </xsl:text>
                        <xsl:apply-templates select="$rdg"/>
                        <xsl:if test="$wit2">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="translate($wit2, '#', '')"/>
                        </xsl:if>
                        <xsl:text>&lt;/span>&lt;/span></xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy>
                            <xsl:apply-templates/>
                        </xsl:copy>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:template>
            

        <xsl:template match="p">
            <p style="text-align: justify;">
                <xsl:apply-templates/>
            </p>
        </xsl:template>
        
        <xsl:template match="head">
            <h2>
                <xsl:apply-templates/> 
            </h2>
        </xsl:template>
        
        <xsl:template match="ref[starts-with(@target, 'http')]">
            <a href="{@target}">
                <xsl:apply-templates/>
            </a>
        </xsl:template>
        
        <!-- Je copie colle le contenu de div1 et de div2 -->
        
        <xsl:template match="div1">
            <div>
                <xsl:apply-templates/>
            </div>
        </xsl:template>
        
        <xsl:template match="div2">
            <div>
                <xsl:apply-templates/>
            </div>
        </xsl:template>
        
        <xsl:template match="span[@rend='italic']">
            <i>
                <xsl:apply-templates/>  
            </i>
        </xsl:template>
        
        <xsl:template match="span[@rend='bold']">
            <b>
                <xsl:apply-templates/>  
            </b>
        </xsl:template>
        
        <xsl:template match="list">
            <ul>
                <xsl:apply-templates/>
            </ul>
        </xsl:template>
       
        <xsl:template match="item">
            <li>
                <xsl:apply-templates/>
            </li>
        </xsl:template>
        
        <!-- Je mets en forme mes notes de bas de page avec renvois  -->
        
        <xsl:variable name="noteCount" select="count(//note)"/>
        
        <xsl:template match="note">
            <sup>
                <a name="note-{generate-id()}" href="#note-ref-{generate-id()}">
                    <xsl:number level="any" count="note"/>
                </a>
                <span class="hover-note">
                    <xsl:value-of select="."/>
                </span>
            </sup>
        </xsl:template>
        
        <xsl:template match="note" mode="footnote">
            <div class="footnote">
                <xsl:variable name="noteIndex">
                    <xsl:number level="any" count="note"/>
                </xsl:variable>
                <sup>
                    <a name="note-ref-{generate-id()}" href="#note-{generate-id()}">
                        <xsl:value-of select="$noteIndex"/>
                    </a>
                </sup>
                <xsl:value-of select="."/>
            </div>
        </xsl:template>


        <!-- Je traduis hi en h2 (h1 trop grand pour mon site) -->
        
        <xsl:template match="hi[@rend='Title']">
            <xsl:choose>
                <xsl:when test="ancestor::div2">
                    <h3>
                        <xsl:apply-templates/>
                    </h3>
                </xsl:when>
                <xsl:otherwise>
                    <h2>
                        <xsl:apply-templates/>
                    </h2>
                </xsl:otherwise>
            </xsl:choose>
          </xsl:template>

        <!-- Je traduis ab en p -->
        
        <xsl:template match="ab">
            <xsl:choose>
                <xsl:when test="descendant::l">
                    <div class="poem">
                        <xsl:apply-templates/>
                    </div>
                </xsl:when>
             <xsl:otherwise>
                <p style='text-align : justify;"'>
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
            </xsl:choose>
        </xsl:template>
        
        <xsl:template match="l">
            <div class="verse">
                <span class="verse-text"><xsl:apply-templates/></span>
                <xsl:variable name="verse-number" select="@n" />
                <xsl:if test="$verse-number mod 5 = 0">
                    <span class="verse-number"><xsl:value-of select="$verse-number" /></span>
                </xsl:if>
            </div>
        </xsl:template>
 
        
        <!-- Je 'ajoute un break après le contenu de reg si dans l pour revenir à la ligne après les vers, sinon rien
             Je m'occupe aussi de réconcilier les mots et ajouter des espaces en réconciliant les lignes -->
        
        <xsl:template match="l/choice/reg[@type='ed']">
            <xsl:choose>
                <xsl:when test="contains(., '¬')">
                    <xsl:value-of select="translate(., '¬', '')"/>
                </xsl:when>
                <xsl:when test="contains(., 'non ulla lege')">
                    <xsl:value-of select="normalize-space(.)"/><xsl:text> </xsl:text>
                </xsl:when>
                <xsl:when test="contains(., 'plus minus')">
                    <xsl:value-of select="normalize-space(.)"/><xsl:text> </xsl:text>
                </xsl:when>
                <xsl:when test="contains(., 'quid restat ? Di.')">
                    <xsl:value-of select="normalize-space(.)"/><xsl:text> </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:template>
        
        
        <xsl:template match="ab//reg[@type='ed' and not(ancestor::l) and not(ancestor::hi[@rend='DropCapital'])]">
            <xsl:choose>
                <xsl:when test="not(contains(., '¬'))">
                    <xsl:apply-templates/><xsl:text> </xsl:text>
                </xsl:when>
                <xsl:when test="contains(., '¬')">
                    <xsl:value-of select="translate(., '¬', '')"/>
                </xsl:when>
            </xsl:choose>
        </xsl:template>
        
        <!--  Attention particulière pour les DropCapital -->
        
        <xsl:template match="hi[@rend='DropCapital']">
            <span class="Drop-capital"><xsl:apply-templates/></span>
        </xsl:template>
        
         <!-- Je découpe le texte par page en introduisant l'élément <fw> -->
        
        <xsl:template match="fw[@type='RunningTitleZone']">
            <p style='text-align : center;"'><xsl:apply-templates/></p>
        </xsl:template>
        <xsl:template match="fw[@type='QuireMarksZone']">
            <p style="text-align: right;"><xsl:apply-templates/></p>
        </xsl:template>
        
        <!-- Je traduis del en span, auquel j'ajoute une classe
        pour ls styler en css -->
        
        <xsl:template match="del">
            <span class="del">
                <xsl:apply-templates/>
            </span>
        </xsl:template>
        
        <!-- Je traduis add en span, auquel j'ajoute une classe
        pour ls styler en css -->
        
        <xsl:template match="add">
            <span class="add">
                <xsl:apply-templates/>
            </span>
        </xsl:template>
        
        <!-- Je traduis ex en span, auquel j'ajoute des
        crochets avant et après -->
        
        <xsl:template match="ex">
            <span class="ex">
                <xsl:text>[</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>]</xsl:text>
            </span>
        </xsl:template>
 
</xsl:stylesheet>