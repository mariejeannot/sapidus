<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        exclude-result-prefixes="xs"
        version="2.0"
        xpath-default-namespace="http://www.tei-c.org/ns/1.0">
        
        <xsl:output method="text" indent="no"/>
        <xsl:strip-space elements="*"/>
        
        <xsl:template match="/">
% !TEX encoding = UTF-8 Unicode
\documentclass[12pt, letterpaper]{article}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{geometry}
\geometry{margin=4cm} %
\usepackage{reledmac}
\usepackage{fontspec}
\usepackage{graphicx}
\usepackage{verse}
\usepackage[right]{lineno}
\setmainfont{Junicode}
\usepackage[utf8]{inputenc}
\title{Édition numérique de l'imprimé comprenant la \textit{Sylua epistolaris seu Barba} de Ioannes Sapidus}
\author{Marie Jeannot-Tirole}
\date{14 juillet 2023}
\newcommand{\resetlinenumbereveryfive}{%
\modulolinenumbers[5]%
\linenumbers%
}
\begin{document}
\maketitle
\pagestyle{fancy}
\fancyhf{}
\lhead{Titre courant}
<xsl:apply-templates/>
\end{document}
</xsl:template>
        
        <xsl:template match="teiHeader"/>
        <xsl:template match="orig"/>
        <xsl:template match="sourceDoc"/>
        <xsl:template match="lb"/>
        <xsl:template match="back"/>
        <xsl:template match="figure"/>
        <xsl:template match="reg[@type='norm']"/>
        <xsl:template match="reg[@type='scansion_xml']"/>
        <xsl:template match="reg[@type='scansion_readable']"/>
        <xsl:template match="fw[@type='RunningTitleZone']"/>
        
            <xsl:template match="anchor"> 
                <xsl:choose>
                    <xsl:when test="ends-with(@xml:id, 'e')">
                        <xsl:text>\footnote{</xsl:text>
                        <xsl:variable name="lem" select="//app[@to=concat('#', current()/@xml:id)]/lem"/>
                        <xsl:variable name="rdg" select="//app[@to=concat('#', current()/@xml:id)]/rdg"/>
                        <xsl:variable name="wit2" select="//app[@to=concat('#', current()/@xml:id)]/rdg/@wit"/>
                        <xsl:variable name="wit1" select="//app[@to=concat('#', current()/@xml:id)]/lem/@wit"/>
                        <xsl:apply-templates select="$lem"/>
                        <xsl:if test="$wit1">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="translate($wit1, '#', '')"/>
                            <xsl:text></xsl:text>
                        </xsl:if>
                        <xsl:text> : </xsl:text>
                        <xsl:apply-templates select="$rdg"/>
                        <xsl:if test="$wit2">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="translate($wit2, '#', '')"/>
                        </xsl:if>
                        <xsl:text>}</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy>
                            <xsl:apply-templates/>
                        </xsl:copy>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:template>
        
        <xsl:template match="hi[@type='Title'and ancestor::div1 and not(ancestor::div2)and not(@rend='DropCapital')]">
            \section{<xsl:apply-templates/>}
        </xsl:template>
        
        <xsl:template match="head">
            \section{<xsl:apply-templates/>}
        </xsl:template>
        
        <xsl:template match="hi[not(@rend='DropCapital')]">

            <xsl:variable name="regText">
                <xsl:for-each select="choice/reg[@type='ed']">
                    <xsl:value-of select="normalize-space(.)"/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
            </xsl:variable>
            
            <xsl:variable name="formattedRegText" select="translate($regText, '¬', '')"/>
            
            <xsl:variable name="finalRegText">
                <xsl:choose>
                    <xsl:when test="substring($formattedRegText, string-length($formattedRegText)) = ' '">
                        <xsl:value-of select="substring($formattedRegText, 1, string-length($formattedRegText) - 1)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$formattedRegText"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            

            <xsl:if test="normalize-space($finalRegText) != ''">
                <xsl:text>\subsection{à compléter à la main}\large{\textbf{\begin{center}</xsl:text>
                <xsl:value-of select="$finalRegText"/>
                <xsl:text>\end{center}}}</xsl:text>
                <xsl:text>&#xa;</xsl:text> <!-- Ajout d'un saut de ligne -->
            </xsl:if>
        </xsl:template>
        
        
        <xsl:template match="reg[@type='ed' and not(ancestor::hi) and not(ancestor::fw) and not(ancestor::l)]">
            <xsl:choose>
                <xsl:when test="contains(., '¬')">
                    <xsl:value-of select="translate(., '¬', '')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(.)"/><xsl:text> </xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:template>
        
        <xsl:template match="reg[@type='ed' and ancestor::l]">
            <xsl:apply-templates/><xsl:text>\\&#xa;</xsl:text>
        </xsl:template>
            
        <xsl:template match="hi[@rend='DropCapital' and not(ancestor::div2)]">
            \textbf{<xsl:apply-templates/>}
        </xsl:template>
        
        <xsl:template match="note">
            \footnote{<xsl:apply-templates/>}
        </xsl:template>
        <xsl:template match="fw[@type='QuireMarksZone']">
            <xsl:text> [</xsl:text><xsl:apply-templates/><xsl:text>] </xsl:text>
        </xsl:template>
        
        <xsl:template match="ex">
                <xsl:text>[</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>]</xsl:text>
        </xsl:template>
        
        <xsl:template match="span[@rend='italic']">
            \textit{<xsl:apply-templates/>}
        </xsl:template>
        
        <xsl:template match="text()">
            <xsl:value-of select="replace(., '&amp;', '\\&amp;')" />
        </xsl:template>
        
        <xsl:template match="list">
            \begin{enumerate}<xsl:apply-templates/>\end{enumerate}           
        </xsl:template>
        
        <xsl:template match="item">
            \item{<xsl:apply-templates/>}          
        </xsl:template>
        
    </xsl:stylesheet>