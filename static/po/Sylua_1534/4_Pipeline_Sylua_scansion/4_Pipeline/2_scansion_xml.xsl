<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="fichier2" select="document('scansion_sylua_xml.xml')"/>
    
    <xsl:template match="tei:div2[@xml:id='Sylua_poem']//tei:l/tei:choice">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="node()"/>
            <xsl:variable name="attribut_n" select="../@n"/>
            <xsl:variable name="matching-l" select="$fichier2//tei:l[@n = $attribut_n]"/>
            <xsl:if test="$matching-l">
                <!-- Ajouter la balise <reg> avec l'attribut type="scansion_xml" -->
                <reg type="scansion_xml">
                    <xsl:apply-templates select="$matching-l/node()"/>
                </reg>
            </xsl:if>
        </xsl:copy>
    </xsl:template>
    
    <!-- Modèle par défaut pour copier les autres éléments tels quels -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
