<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:output method="xml" indent="yes" encoding="utf-8"/>
    
    <!-- Importe les URL corrigées à partir d'un fichier texte -->
    <xsl:param name="urlFile" select="'liste_urls.txt'"/>
    
    <!-- Modifie les URL des balises <graphic> avec les URL corrigées -->
    <xsl:template match="graphic">
        <xsl:variable name="index" select="position()"/>
        <xsl:variable name="urls" select="document($urlFile)/urls/url"/>
        
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="url">
                <xsl:value-of select="$urls[$index]"/>
            </xsl:attribute>
        </xsl:copy>
    </xsl:template>
    
    <!-- Copie les autres éléments et attributs sans modification -->
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
