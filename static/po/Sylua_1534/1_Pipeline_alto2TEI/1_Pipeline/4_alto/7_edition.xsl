<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
        <xsl:template match="@*|node()">
            <xsl:copy>
                <xsl:apply-templates select="@*|node()"/>
            </xsl:copy>
        </xsl:template>
        
        <!-- balise <reg> : je la copie deux fois et je mets à chaque fois un attribut différent -->
        <xsl:template match="reg">
            <xsl:copy>
                <xsl:attribute name="type">normalization</xsl:attribute>
                <xsl:apply-templates select="@*|node()"/>
            </xsl:copy>
            <xsl:copy>
                <xsl:attribute name="type">edition</xsl:attribute>
                <xsl:apply-templates select="@*|node()"/>
            </xsl:copy>
        </xsl:template>
    

</xsl:stylesheet>
