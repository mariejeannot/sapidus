<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:output method="xml" indent="yes" encoding="utf-8"/>
        
        <xsl:template match="@*|node()">
            <xsl:copy>
                <xsl:apply-templates select="@*|node()"/>
            </xsl:copy>
        </xsl:template>
    
    <!-- quand la balise <lb> est précédé par une balise <reg> dans <choice> contenant ¬ ATTENTION : ne fonctionne pas sur les changements de page, je ne l'ai pas inclu->
        
    <xsl:template match="lb">
        <xsl:variable name="precedingChoice" select="preceding-sibling::choice[1]" />
        <xsl:choose>
            <xsl:when test="$precedingChoice and $precedingChoice/reg[@type='ed' and contains(., '¬')]">
                <xsl:copy>
                    <xsl:attribute name="break">no</xsl:attribute>
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    

    
    
    
    
</xsl:stylesheet>
