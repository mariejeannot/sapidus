<?xml version="1.0" encoding="UTF-8"?>

<!-- Source XSL: Pauline Jacsont, adaptation 1: Elodie Paupe, adaptation 2 : Simon Gabay, adaptation 3 : Marie Jeannot-Tirole -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.loc.gov/standards/alto/ns-v4#" version="2.0"
    exclude-result-prefixes="xs">
    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"
        xpath-default-namespace="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"/>

    <xsl:strip-space elements="*"/>
    
    <!-- CHANGE VARIABLE -->

    <xsl:variable name="nma">https://opendata2.uni-halle.de//iiif-imgsrv/</xsl:variable> <!-- Serveur iiif où j'ai pris le doc -->
    <!-- <xsl:variable name="library">12148</xsl:variable> -->
    <xsl:variable name="document">4ac17a17-9fd2-4ddb-a6f6-fde1ac747b6f</xsl:variable> 
    <xsl:variable name="fileName">Apologia_1534</xsl:variable> 
    <xsl:variable name="xmlDocuments" select="collection(concat('../3_alto/', $fileName,'/?select=?*.xml;recurse=yes'))"/>
   
   <!-- <xsl:variable name="xmlDocuments"
        select="collection('../alto/Tyard_1549_btv1b8617188p/?select=?*.xml;recurse=yes')"/> -->

    <xsl:template match="/" >
        <xsl:processing-instruction name="xml-model">
            <xsl:text>href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:text>
        </xsl:processing-instruction>
        <xsl:processing-instruction name="xml-model">
            <xsl:text>href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:text>
        </xsl:processing-instruction>
        <TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="{$fileName}">
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <title>Édition numérique de la <span rend="italic">Sylua epistolaris seu Barba</span> de <persName ref="#Sap">Johann Sapidus</persName>.
                        </title>
                        <author xml:id="MJT">
                            <persName instant="false" full="yes">
                                <forename full="yes">Marie</forename>
                                <surname full="yes">Jeannot-Tirole</surname>
                                <ptr type="orcid" target="0000-0002-6607-653X"/>
                            </persName>
                        </author>
                        <respStmt>
                            <resp>Transcription, segmentation, édition</resp>
                            <persName ref="#MJT" instant="false" full="yes"/>
                        </respStmt>
                        <respStmt>
                            <resp>Scripts de conversion du format ALTO vers TEI</resp>
                            <persName xml:id="PJ" instant="false" full="yes">
                                <forename full="yes">Pauline</forename>
                                <surname full="yes">Jacsont</surname>
                                <ptr type="orcid" target="0000-0002-6296-3246"/>
                            </persName>
                            <persName xml:id="EP" instant="false" full="yes">
                                <forename full="yes">Elodie</forename>
                                <surname full="yes">Paupe</surname>
                            </persName>
                            <persName xml:id="SG" instant="false" full="yes">
                                <forename full="yes">Simon</forename>
                                <surname full="yes">Gabay</surname>
                                <ptr type="orcid" target="0000-0001-9094-4475"/>
                            </persName>
                            <persName ref="#MJT" instant="false" full="yes"/>
                        </respStmt>
                        <respStmt>
                            <resp>Scripts de conversion du format TEI vers txt et HTML</resp>
                            <persName ref="#MJT" instant="false" full="yes"/>
                        </respStmt>
                    </titleStmt>
                    <extent>
                        <measure unit="images" n="87">87 pages avec du contenu textuel. Nous n'avons pas intégré les pages vierges de l'édition.</measure>
                        <measure unit="XML-line" n=""/> <!-- à (faire) compter à la fin ££ -->
                    </extent>
                    <publicationStmt>
                        <authority>Université de Genève</authority>
                        <address>
                            <addrLine>rue du Général-Dufour, 24</addrLine>
                            <addrLine>1211 Genève</addrLine>
                            <addrLine>Suisse</addrLine>
                        </address>
                        <availability status="restricted" n="CC-BY-NC-SA" default="false">
                            <licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/"/>
                            <p part="N">Creative Commons Attribution 4.0 CC-BY-NC-SA 4.0</p>
                        </availability>
                        <date when="2023-04-18" instant="false"/>
                    </publicationStmt>
                    <sourceDesc default="false"> <!-- à quoi sert cet attribut ? Pas trouvé dans le Guidelines -->
                        <bibl default="false">
                            <ptr target="https://opendata2.uni-halle.de//handle/1516514412012/177"/>
                            <author xml:id="Sapidus">
                                <persName instant="false" full="yes">
                                    <forename full="yes" lang="lat">Ioannes</forename>
                                    <surname full="yes" lang="lat">Sapidus</surname>
                                    <forename full="yes" lang="all">Hans</forename>
                                    <surname full="yes" lang="all">Witz</surname>
                                    <ptr type="isni" target="0000 0000 7244 0292">https://isni.org/isni/0000000072440292</ptr>
                                </persName>
                            </author>
                            <title>APOLOGIA IOAN. PIERII VALERIANI BElunen. Medicum famniliæ Rhetoris. Pro Sacerdotum Barbis. SYLVA EPISTOLARIS SEV BARba loan. Sapidi, propter argumenti cognationem adiecta.</title>
                            <pubPlace type="GeoNames" target="2973783" ref="https://www.geonames.org/2973783/strasbourg.html">Strasbourg</pubPlace> <!-- voir si utile de remettre tous les lieux à la fin -->
                            <publisher xml:id="Albrecht">
                                <persName instant="false" full="yes">
                                    <forename full="yes" lang="lat">Ioannes</forename>
                                    <surname full="yes" lang="lat">Albertus</surname>
                                    <forename full="yes" lang="all">Hans</forename>
                                    <surname full="yes" lang="all">Geisser</surname>
                                    <forename full="yes" lang="fr">Johann</forename>
                                    <surname full="yes" lang="fr">Albrecht</surname>
                                    <ptr type="isni" target="0000 0000 7244 0292">https://isni.org/isni/0000000072440292</ptr>
                                </persName>
                            </publisher>
                            <date cert="high" when="1534-02" resp="colophon" instant="false"/> <!-- comment dire que l'édition a été imprimée entre février et mars ? -->
                        </bibl>
                        <msDesc xml:id="msD">
                            <msIdentifier>
                                <country key="DE"/>
                                <settlement>Halle</settlement>
                                <repository>Universitäts- und Landesbibliothek Sachsen-Anhalt</repository>
                                <idno type="cote">Ung III A 170 (2)</idno>
                                <idno type="URI">https://opendata2.uni-halle.de//handle/1516514412012/177</idno>
                                <idno type="URI">http://dx.doi.org/10.25673/opendata2-175</idno>                             
                            </msIdentifier>
                            <physDesc>
                                <objectDesc>
                                    <p part="N">Édition imprimée</p>
                                </objectDesc>
                            </physDesc>
                        </msDesc>
                        <listWit>
                            <witness ref="#msD"/>
                        </listWit>
                    </sourceDesc>
                </fileDesc>
                <profileDesc>
                    <langUsage default="false">                     <!-- ici, la langue de mon document ? -->
                        <language ident="fr">français</language>
                    </langUsage>
                    <textClass default="false">
                        <keywords>
                            <term type="genre" n="poetry|letter"/>
                            <term type="form" n="verse"/>
                            <term type="corrected" n="partially"/> <!-- pour le moment, à corriger version finale -->
                        </keywords>
                    </textClass>
                </profileDesc>
                <encodingDesc>
                    <appInfo>
                        <application ident="Kraken" version="4.1">
                            <label>Kraken</label>
                            <ptr target="https://github.com/mittagessen/kraken"/>
                        </application>
                    </appInfo>
                    <classDecl>
                        <taxonomy xml:id="SegmOnto">
                            <bibl default="false" status="draft">
                                <title>SegmOnto</title>
                                <ptr target="https://github.com/segmonto"/>
                            </bibl>
                            <category xml:id="SegmOntoZones"/>
                            <category xml:id="SegmOntoLines"/>
                        </taxonomy>
                    </classDecl>
                </encodingDesc>
                <revisionDesc status="draft">
                    <change when="2023-05-12" who="#MJT" status="draft">Conversion ALTO to TEI</change>
                </revisionDesc>
            </teiHeader>
            <sourceDoc xml:id="transcription">
                <xsl:for-each select="$xmlDocuments">
                    <xsl:for-each select="//alto">
                        <!-- Page -->
                        <!-- Select the page number -->
                        <xsl:variable name="page" select="substring-before(self::alto/Description/sourceImageInformation/fileName, '.')"/>
                        <xsl:element name="surface">
                            <!-- ID -->
                            <xsl:attribute name="xml:id">
                                <xsl:text>f</xsl:text>
                                <xsl:value-of select="$page"/>
                            </xsl:attribute>
                            <!-- Coordinates -->
                            <xsl:attribute name="ulx">
                                <xsl:value-of select="//Page/PrintSpace/@HPOS"/>
                            </xsl:attribute>
                            <xsl:attribute name="uly">
                                <xsl:value-of select="//Page/PrintSpace/@VPOS"/>
                            </xsl:attribute>
                            <xsl:attribute name="lrx">
                                <xsl:value-of select="//Page/PrintSpace/@WIDTH"/>
                            </xsl:attribute>
                            <xsl:attribute name="lry">
                                <xsl:value-of select="//Page/PrintSpace/@HEIGHT"/>
                            </xsl:attribute>
                            <!-- Add image's ark -->
                            <xsl:element name="graphic">
                                <xsl:attribute name="url">
                                    <xsl:value-of select="concat($nma,'ark:/f',$page,'/full/full/0/native.jpg')"/> <!-- à modifier -->
                                </xsl:attribute>
                            </xsl:element>
                            <!-- Regions -->
                            <xsl:for-each select="//TextBlock">
                                <xsl:element name="zone">
                                    <xsl:attribute name="xml:id">
                                        <xsl:text>f</xsl:text><xsl:value-of select="$page"/><xsl:text>_</xsl:text><xsl:value-of select="@ID"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="type">
                                        <!-- Attraper la valeur codée -->
                                        <xsl:variable name="type_zone">
                                            <xsl:value-of select="@TAGREFS"/>
                                        </xsl:variable>
                                        <!-- Recherche de la véritable valeur exprimée -->
                                        <xsl:variable name="type_zone_valeur">
                                            <xsl:value-of
                                                select="//OtherTag[@ID = $type_zone]/@LABEL"/>
                                        </xsl:variable>
                                        <xsl:value-of select="$type_zone_valeur"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="n">
                                        <xsl:number level="single" count="." format="1"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="ulx">
                                        <xsl:value-of select="@HPOS"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="uly">
                                        <xsl:value-of select="@VPOS"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="lrx">
                                        <xsl:value-of select="@WIDTH"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="lry">
                                        <xsl:value-of select="@HEIGHT"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="points">
                                        <xsl:variable name="value" select="./Shape/Polygon/@POINTS"/>
                                        <xsl:analyze-string select="$value"
                                            regex="([0-9]+)\s([0-9]+)">
                                            <xsl:matching-substring>
                                                <xsl:for-each select="$value">
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  <xsl:text>,</xsl:text>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  <xsl:text> </xsl:text>
                                                </xsl:for-each>
                                            </xsl:matching-substring>
                                        </xsl:analyze-string>
                                    </xsl:attribute>
                                    <xsl:attribute name="source">
                                        <xsl:value-of select="$nma"/>
                                        <xsl:text>/</xsl:text>
                                        <xsl:value-of select="@HPOS"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="@VPOS"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="@WIDTH"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="@HEIGHT"/>
                                        <xsl:text>/full/0/default.jpg</xsl:text>
                                    </xsl:attribute>
                                    <!-- Ajouter les lignes -->
                                    <xsl:for-each select="TextLine">
                                        <xsl:element name="zone">
                                            <xsl:attribute name="xml:id">
                                                <xsl:text>f</xsl:text><xsl:value-of select="$page"/><xsl:text>_</xsl:text><xsl:value-of select="@ID"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:text>segmontoLine</xsl:text>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <!-- Attraper la voiture -->
                                                <xsl:variable name="type_zone">
                                                  <xsl:value-of select="@TAGREFS"/>
                                                </xsl:variable>
                                                <!-- Recherche de la véritable valeur -->
                                                <xsl:variable name="type_zone_valeur">
                                                  <xsl:value-of
                                                  select="//OtherTag[@ID = $type_zone]/@LABEL"/>
                                                </xsl:variable>
                                                <xsl:value-of select="$type_zone_valeur"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="n">
                                                <xsl:number level="single" count="." format="1"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="points">
                                                <xsl:variable name="value"
                                                  select="./Shape/Polygon/@POINTS"/>
                                                <xsl:analyze-string select="$value"
                                                  regex="([0-9]+)\s([0-9]+)">
                                                  <xsl:matching-substring>
                                                  <xsl:for-each select="$value">
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  <xsl:text>,</xsl:text>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  <xsl:text> </xsl:text>
                                                  </xsl:for-each>
                                                  </xsl:matching-substring>
                                                </xsl:analyze-string>
                                            </xsl:attribute>
                                            <xsl:attribute name="source">
                                                <xsl:value-of select="concat($nma,'iiif/ark:/f',$page,'/')"/> <!-- à modifier-->
                                                <xsl:value-of select="@HPOS"/>
                                                <xsl:text>,</xsl:text>
                                                <xsl:value-of select="@VPOS"/>
                                                <xsl:text>,</xsl:text>
                                                <xsl:value-of select="@WIDTH"/>
                                                <xsl:text>,</xsl:text>
                                                <xsl:value-of select="@HEIGHT"/>
                                                <xsl:text>/full/0/default.jpg</xsl:text>
                                            </xsl:attribute>
                                            <!-- Baseline -->
                                            <xsl:element name="path">
                                                <xsl:variable name="nbaseline">
                                                  <xsl:number level="single" count="." format="1"/>
                                                </xsl:variable>
                                                <xsl:attribute name="n">
                                                  <xsl:value-of select="$nbaseline"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="xml:id">
                                                  <xsl:value-of
                                                      select="concat('f',$page,'_',@ID, '_baseline_', $nbaseline)"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="type">
                                                  <xsl:text>baseline</xsl:text>
                                                </xsl:attribute>
                                                <xsl:attribute name="points">
                                                  <xsl:variable name="value" select="@BASELINE"/>
                                                  <xsl:analyze-string select="$value"
                                                  regex="([0-9]+)\s([0-9]+)">
                                                  <xsl:matching-substring>
                                                  <xsl:for-each select="$value">
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  <xsl:text>,</xsl:text>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  <xsl:text> </xsl:text>
                                                  </xsl:for-each>
                                                  </xsl:matching-substring>
                                                  </xsl:analyze-string>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <!-- Ajouter la transcription -->
                                            <xsl:element name="line">
                                                <xsl:variable name="nline">
                                                  <xsl:number level="single" count="." format="1"/>
                                                </xsl:variable>
                                                <xsl:attribute name="xml:id">
                                                  <xsl:value-of
                                                      select="concat('f',$page,'_',@ID, '', $nline)"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="String/@CONTENT"/>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:for-each>
            </sourceDoc>
            <xsl:element name="text">
                <front>
                    <div xml:id="intro">
                        <head>Introduction</head>
                        <p xml:id="intro_p1">Mettre les commentaires de mon édition</p>
                    </div>
                    <div xml:id="établissement">
                        <head>Introduction</head>
                        <p>Mettre les commentaires de mon édition en renvoyant à l'<span type="ref"
                            target="#intro_p1">introduction</span></p>
                    </div>
                </front>
                <xsl:element name="body">
                    <xsl:element name="p"/>
                </xsl:element>
            </xsl:element>
        </TEI>
    </xsl:template>

</xsl:stylesheet>
