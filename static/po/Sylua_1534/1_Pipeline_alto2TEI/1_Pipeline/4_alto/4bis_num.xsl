<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>

    <!-- <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    Correspondance sur les balises <l> 
    <xsl:template match="body//l">
        <xsl:variable name="versNum" select="position()"/>
        <xsl:copy>
            <xsl:attribute name="xml:id">
                <xsl:text>v</xsl:text>
                <xsl:value-of select="$versNum"/>
            </xsl:attribute>
            <xsl:attribute name="n">
                <xsl:value-of select="$versNum"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template> -->

    <!-- Correspondance sur le corps du texte -->
    <xsl:template match="body">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Correspondance sur toutes les balises <l> dans le corps du texte -->
    <xsl:template match="body//l">
        <xsl:copy>
            <!-- Ajout du numéro dans l'attribut @n -->
            <xsl:attribute name="n">
                <xsl:value-of select="count(preceding::l) + 1"/>
            </xsl:attribute>
            <!-- Ajout du numéro dans l'attribut @xml:id -->
            <xsl:attribute name="xml:id">
                <xsl:text>v</xsl:text>
                <xsl:value-of select="count(preceding::l) + 1"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Modèle de correspondance par défaut pour copier les autres éléments tels quels -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
