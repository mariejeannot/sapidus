<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="surface[ancestor::body]">
        <xsl:element name="pb" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="corresp"><xsl:value-of select="@xml:id"/></xsl:attribute>
        </xsl:element> 
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="//body/surface/zone">
        <xsl:choose>
            <xsl:when test="@type='MainZone'">
                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>
            <xsl:when test="@type='MainZone_vers'">
                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>
            <xsl:when test="@type='TitlePageZone'">
                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>
            <!--
            <xsl:when test="@type='MarginTextZone'">
                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when> 
            <xsl:when test="@type='DropCapitalZone'">
                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when> -->
            <xsl:when test="@type='NumberingZone'">
                <xsl:element name="fw" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>
            <xsl:when test="@type='QuireMarksZone'">
                <xsl:element name="fw" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>
            <xsl:when test="@type='RunningTitleZone'">
                <xsl:element name="fw" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>  
            <xsl:when test="@type='GraphicZone'">
                <xsl:element name="graphic" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
                    <xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>      
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="//body/surface/zone/zone">
        <xsl:choose>
            <xsl:when test="@type='DefaultLine'">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:when test="@type='CustomLine'">
                <xsl:element name="l" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@type='DropCapitalLine'">
                <xsl:element name="hi" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@type='HeadingLine'">
                <xsl:element name="hi" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="line[ancestor::body]">
        <xsl:element name="lb" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="corresp"><xsl:text>#</xsl:text><xsl:value-of select="@xml:id"/></xsl:attribute>
            <xsl:attribute name="type">OCRline</xsl:attribute>
        </xsl:element>
        <xsl:element name="choice" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:element name="orig" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:apply-templates/>
            </xsl:element>
            <xsl:element name="reg" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="path[ancestor::body]"/>

    <xsl:template match="graphic[ancestor::body]"/>
    
    
</xsl:stylesheet>