---
title: "Topic_Modeling_Sylua"
author: "Alice Leflaëc (orig Topic_Modeling_Levitique) et Marie Jeannot-Tirole (mod Topic_Modeling_Sylua)"
date: "juin 2023"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# I. Préparation des données

## A. Définition de la session de travail
Indication du chemin vers le notebook
```{r}
setwd("~/GitHub/sapidus/static/Sylua/5_scripts_R/Topic_Modeling")
monDossier="~/GitHub/sapidus/static/Sylua/5_scripts_R/Topic_Modeling"
```

## B. Récupération des textes
Les textes latins bruts ont préalablement été placés en format .txt dans le dossier de travail (sous-dossier Textes)
Le texte de la *Sylua epistolaris seu Barba* a été récupéré en format txt par export de mon édition XML-TEI (Apologia_1534.xml).
Le texte du *Pro Sacerdotum barbis* a été récupéré de la même manière.

```{r}
# Fonction readLines indique le contenu de la variable sous forme de chaîne de caractères. 
#Attention : sauter une ligne après le texte final pour éviter l'erreur "incomplete final line found in...."
barba_Sylua <-readLines("Textes/Sylua.txt")
barba_Pro <-readLines("Textes/Pro_sacerdotum_barbis.txt")
```

## C. Premier nettoyage du texte : suppression de la ponctuation
On procède à un premier nettoyage du texte avant la lemmatisation en supprimant de la ponctuation.

Création d'un dossier Clearer pour ranger les textes plus propres.
dir.create dans une cellule R ou création d'un dossier dans les Documents de l'ordinateur.

Utilisation d'une fonction pour éliminer la ponctuation encombrante avant la lemmatisation (la fonction removePunctation est difficilement exploitable dans le dataframe, il est plus simple de l'utiliser avant). Il faut installer la library tm.

```{r}
if(!require("tm")){
  install.packages("tm")
  library("tm")
}
#On élimine la ponctuation du texte.
barba_Sylua_clearer <- removePunctuation(barba_Sylua)
barba_Pro_clearer <- removePunctuation(barba_Pro)
#Comme la ponctuation sera enlevée sans sauvegarder le résultat, il faut créer un nouveau fichier qui ira dans Clearer.
write(barba_Sylua_clearer, file = "Clearer/barba_Sylua_clearer.txt")
# On indique le nom du fichier et le chemin pour la nouvelle variable.
write(barba_Pro_clearer, file = "Clearer/barba_Pro_clearer.txt")
```

Les documents peuvent maintenant être lemmatisés sur Pyrrha et exportés en format .tsv.

# II. à partir du fichier lemmatisé

## A. Second nettoyage du texte : retrait des _stopwords_

###  1. Importation de la liste de _stopwords_
 
```{r}
Stopwords <- read.csv("stopword_lat.csv", header=FALSE, stringsAsFactors=FALSE)[,]
head(Stopwords,10)
```

### 2. Nettoyage du texte
 
Il est nécessaire de créer d'abord une chaîne de caractères.

#### 2. a) Pour la Sylua
```{r}
df_Sylua <- read.table("Lemma/sylua-clean2206.tsv", header = TRUE, sep = "\t")
#Création d'une chaîne de caractère vide qui contiendra à l'avenir tous les textes contenus dans df. 
#[Dans le cours de Simon, la colomme Lemma est déjà une chaîne de caractères. On ruse donc pour obtenir cette chaîne (elle est vide pour l'instant mais on dira à R : si le mot n'apparaît pas dans les stopwords ou dans la ponctuation, tu le mets dans cette chaîne de caractères).]
chaîne_de_caractères_Sylua <- ""
#Laisser le contenu vide : il n'y a rien pour l'instant.
```

#### 2.b) Pour le Pro sacerdotum barbis
```{r}
df_Pro <- read.table("Lemma/pro-sacerdotum-barbis220623.tsv", header = TRUE, sep="\t")
chaîne_de_caractères_Pro <- ""
#Laisser le contenu vide : il n'y a rien pour l'instant.
chaîne_de_caractères_Sylua
#Taper le nom de la variable me permet de l'afficher.
```

Puis on indique le contenu de la chaîne de caractères

# Réduction à la minuscule et retrait des stopwords 
On indique qu'on réduit à la minuscule pour chaque mot à l'aide d'une boucle, qu'on ne prend pas en compte les stopwords puis qu'on ôte la ponctuation (en réalité la ponctuation avait été enlevée précédemment, mais je laisse le code ici pour le garder en mémoire).

#Pour la Sylua
```{r}
for (word in tolower(df_Sylua$lemma)) {
  if (!word %in% Stopwords) {
    chaîne_de_caractères_Sylua <- paste(chaîne_de_caractères_Sylua, word, sep=" ")
  }
  #J'enlève la ponctuation.
  chaîne_de_caractères_Sylua <- gsub("[[:punct:]]", "", chaîne_de_caractères_Sylua)
}
chaîne_de_caractères_Sylua
```


#Pour le Pro Sacerdotum Barbis
```{r}
for (word in tolower(df_Pro$lemma)) {
  if (!word %in% Stopwords) {
    chaîne_de_caractères_Pro <- paste(chaîne_de_caractères_Pro, word, sep=" ")
  }
  #J'enlève la ponctuation.
  chaîne_de_caractères_Pro <- gsub("[[:punct:]]", "", chaîne_de_caractères_Pro)
}
chaîne_de_caractères_Pro
```


## III. Une approche _bag_of_words_

On divise le texte en cinq morceaux (5 est un chiffre arbitraire, on peut en mettre plus ou moins. Ici, j'ai 4957 mots, donc je fais des paquets de 1000 mots) et on met ces cinq morceaux dans une liste qu'on peut par exemple baptiser Extraits. C'est ce qui permet l'approche _bag_of_words_.

Approche _bag_of_words_ : idée que le monde peut être décrit au moyen d'un dictionnaire. Dans sa version la plus simple, un document particulier est représenté par l'histogramme des occurrences des mots le composant : pour un document donné, chaque mot se voit affecté le nombre de fois qu'il apparaît dans le document (source : Wikipédia).

## A. Création d'une liste

### 1. Pour la Sylua

```{r}
Nb_sequences <- 5
Extraits_Sylua <- strwrap(chaîne_de_caractères_Sylua, nchar(chaîne_de_caractères_Sylua) / Nb_sequences)
#On peut afficher le contenu de chaque séquence :
Extraits_Sylua[1]
```

## 2. Pour le Pro Sacerdotum barbis

Il contient plus de mots que la *Sylua* (8433 mots), d'où le choix de huit.

```{r}
Nb_sequences <- 8
Extraits_Pro <- strwrap(chaîne_de_caractères_Pro, nchar(chaîne_de_caractères_Pro) / Nb_sequences)
Extraits_Pro[3]
```

## B. Transformation en matrice vectorielle

Il faut installer les packages tm et tidytext.

### 1. Pour la Sylua

```{r}
if(!require("tm")){
  install.packages("tm")
  library("tm")
}
if(!require("tidytext")){
  install.packages("tidytext")
  library("tidytext")
}

#Je transforme mes textes en corpus avec la fonction `corpus()`, un objet de classe `corpus` manipulable dans `R` contenant des données et des métadonnées.
#La fonction `VectorSource` transforme chaque document en vecteur.
corpus_Sylua <- Corpus(VectorSource(Extraits_Sylua), readerControl = list(language = "lat"))
# J'affiche les informations à propos de ce corpus
corpus_Sylua
```

### 2. Pour le Pro Sacerdotum barbis

```{r}
if(!require("tm")){
  install.packages("tm")
  library("tm")
}
if(!require("tidytext")){
  install.packages("tidytext")
  library("tidytext")
}

corpus_Pro <- Corpus(VectorSource(Extraits_Pro), readerControl = list(language = "lat"))

corpus_Pro
```

## C. Création d'un _document_term_matrix_

Un _document_term_matrix_ est une matrice mathématique qui décrit la fréquence des termes qui apparaissent dans une collection de documents.

### 1. Pour la Sylua
```{r}
dtm_Sylua <- DocumentTermMatrix(corpus_Sylua)
dtm_Sylua
```

### 2. Pour le Pro sacerdotum barbis
```{r}
dtm_Pro <- DocumentTermMatrix(corpus_Pro)
dtm_Pro
```

#IV. Analyse des données : fréquence des termes

## A.Graphe représentant la fréquence des termes

Installation de la library pour le graphe et dessin du graphe

### 1. Pour la Sylua¨

as.data.frame est une fonction vérifiant qu'un objet est un dataframe ou le forçant à le devenir si c'est possible.
colSums est une fonction permettant de former des sommes et des moyennes de lignes et de colonnes pour des tableaux et des dataframes.
as.matrix est une fonction générique convertissant en matrice.
colnames récupère ou définit le nom des lignes et des colonnes dans un objet de type matrice.
c est une fonction générique qui combine ses arguments. La méthode par défaut combine les arguments pour former un vecteur.

```{r}
freq_Sylua <- as.data.frame(colSums(as.matrix(dtm_Sylua)))
colnames(freq_Sylua) <- c("frequence")
```

Pour dessiner un graphe, nécessité d'installer une nouvelle library: `ggplot2`
gg = Grammar of Graphics
Avec ggplot 2, les données représentées graphiquement proviennent toujours d'un dataframe.

```{r}
if (!require("ggplot2")){
  install.packages("ggplot2")
  library("ggplot2")
}
```

Dessin du graphe
La fonction ggplot initialise le graphique. On commence par définir la source des données (ici freq_Sylua), puis on indique quelle donnée on veut représenter (les attributs esthétiques) en passant des arguments dans la fonction aes(). Cette fonction spécifie les variables à visualiser et associe à chaque variable un emplacement ou un rôle: on renseigne le paramètre x qui est la variable à représenter sur l'axe horizontal (ici la fréquence).
On ajoute, enfin, les éléments de représentation graphique (= geom). On les ajoute à l'objet graphique de base avec l'opérateur +. geom_density permet d'afficher l'estimation de densité d'une variable numérique. On crée une courbe de distribution.
Source de la plupart des explications : https://juba.github.io/tidyverse/08-ggplot2.html


```{r}
ggplot(freq_Sylua, aes(x=frequence)) + geom_density()
```



### 2. Pour le Pro Sacerdotum Barbis
```{r}
freq_Pro <- as.data.frame(colSums(as.matrix(dtm_Pro)))
colnames(freq_Pro) <- c("frequence")

#Dessin du graphe
ggplot(freq_Pro, aes(x=frequence)) + geom_density()
```

## B. Analyse des données
 
 On retrouve la loi de Zipf dans la distribution des données.
 
### 1. Mots avec de faibles fréquences
On peut compter les mots avec les fréquences faibles, par exemple avec moins de 10 occurrences (n+1).

#### 1. a) Pour la Sylua
```{r}
motsPeuFrequents_Sylua <- findFreqTerms(dtm_Sylua, 0, 9)
#Si vous êts sur windows, décommentez la ligne suivante
#Encoding(motsPeuFrequents)<-"latin-1"
length(motsPeuFrequents_Sylua)
head(motsPeuFrequents_Sylua,50)
```

#### 2. b) Pour le Pro sacerdotum barbis
```{r}
#Je retire tous les mots qui apparaissent très peu.
motsPeuFrequents_Pro<- findFreqTerms(dtm_Pro, 0, 9)
#Si vous êts sur windows, décommentez la ligne suivante
#Encoding(motsPeuFrequents)<-"latin-1"
length(motsPeuFrequents_Pro)
head(motsPeuFrequents_Pro,50)
```

### 2. Mots avec de fortes fréquences
 On peut aussi compter et afficher les mots les plus fréquents, par exemple avec plus de 19 occurrences (deux exemples pour chaque oeuvre).
 Selon les résultats attendus, on peut moduler les fréquences recherchées.

#### 2. a) Pour la Sylua
```{r}
motsTresFrequents_Sylua <- findFreqTerms(dtm_Sylua, 19, Inf)
#Si vous êts sur windows, décommentez la ligne suivante
#Encoding(motsTresFrequents)<-"latin-1"
length(motsTresFrequents_Sylua)
head(motsTresFrequents_Sylua,50)
```

```{r}
motsTresFrequents_Sylua <- findFreqTerms(dtm_Sylua, 29, Inf)
#Si vous êts sur windows, décommentez la ligne suivante
#Encoding(motsTresFrequents)<-"latin-1"
length(motsTresFrequents_Sylua)
head(motsTresFrequents_Sylua,50)
```

#### 2. b) Pour le Pro Sacerdotum barbis
```{r}
motsTresFrequents_Pro <- findFreqTerms(dtm_Pro, 9, Inf)
#Si vous êts sur windows, décommentez la ligne suivante
#Encoding(motsTresFrequents)<-"latin-1"
length(motsTresFrequents_Pro)
head(motsTresFrequents_Pro,50)
```

```{r}
motsTresFrequents_Pro <- findFreqTerms(dtm_Pro, 11, Inf)
#Si vous êts sur windows, décommentez la ligne suivante
#Encoding(motsTresFrequents)<-"latin-1"
length(motsTresFrequents_Pro)
head(motsTresFrequents_Pro,50)
```

### 3. Association entre les mots
#### 3. a) Pour la Sylua

Pour le moment, "barba, "uir" et "ego".

```{r}
findAssocs(dtm_Sylua, terms = "barba", corlimit = 0.5)
```
```{r}
findAssocs(dtm_Sylua, terms = "uir", corlimit = 0.5)
```
```{r}
findAssocs(dtm_Sylua, terms = "ego", corlimit = 0.5)
```

#### 3. a) Pour le Pro Sacerdotum barbis

Pour le moment, "barba, "uir" et "ego".

```{r}
findAssocs(dtm_Pro, terms = "barba", corlimit = 0.5)
```
```{r}
findAssocs(dtm_Pro, terms = "uir", corlimit = 0.5)
```
```{r}
findAssocs(dtm_Pro, terms = "ego", corlimit = 0.5)
```

 
### 4. Nettoyage de la DTM (DocumentTermMatrix) pour éliminer les rangs vides.

££ à quoi ça sert ?

##### 4. a) Pour la Sylua
```{r}
rowTotals <- apply(dtm_Sylua, 1, sum)      #On trouve la somme des mots dans chaque document.
dtm_Sylua_clean   <- dtm_Sylua[rowTotals> 0, ]    #On retire tous les documents sans mot.
```

#### 4. b) Pour le Pro sacerdotum barbis
```{r}
rowTotals <- apply(dtm_Pro, 1, sum)
dtm_Pro_clean   <- dtm_Pro[rowTotals> 0, ]
```

#V. Topic Modeling

Un thème ( _topic_ ) est un _cluster_ de mots i.e. une récurrence de co-occurrences.

## A. Installation de la library pour le _topic_modeling_

```{r}
if(!require("topicmodels")){
  install.packages("topicmodels")
  library("topicmodels")
}
```
 
### 1. LDA ( _Latent Dirichlet allocation_ )
 
 La _LDA_  est un modèle génératif probabiliste permettant d’expliquer des ensembles d’observations au moyen de groupes non observés, eux-mêmes définis par des similarités de données. Le modèle va classer aléatoirement tous les mots en _n_ sujets, et tenter d'affiner cette répartition de manière itérative en observant les contextes.
Il faut définir à l'avance un nombre de sujets/thèmes ( cf. infra la variable `k`).
 
#### 1. a) Pour la Sylua
```{r}
#On peut partir sur une classification en deux _topics_.
k = 2
lda_2_Sylua <- LDA(dtm_Sylua_clean, k= k, control = list(seed = 1234))
#Seed doit être un nombre aléatoire.
#Puis on peut tenter une classification en trois topics.
lda_3_Sylua <- LDA(dtm_Sylua_clean, k= k+1, control = list(alpha = 0.1))
```

Le résultat produit est une matrice avec pour chaque mot la probabilité qu'il appartienne à l'un des différents _topics_. On donne un score _β_, qui est celui présenté infra (il contient les probabilités de chaque mot d’avoir été généré par un topic).

```{r}
topics_2_Sylua <- tidy(lda_2_Sylua, matrix = "beta")
#Tidy est une fonction rangeant le résultat d'un test dans un dataframe récapitulatif.
topics_2_Sylua
```

```{r}
topics_3_Sylua <- tidy(lda_3_Sylua, matrix = "beta")
topics_3_Sylua
```


#### 1. b) Pour le Pro Sacerdotum barbis

```{r}
#On peut partir sur une classification en deux _topics_.
k = 2
lda_2_Pro <- LDA(dtm_Pro_clean, k= k, control = list(seed = 1234))
#Seed doit être un nombre aléatoire.
#Puis on peut tenter une classification en trois topics.
lda_3_Pro <- LDA(dtm_Pro_clean, k= k+1, control = list(alpha = 0.1))
```

Le résultat produit est une matrice avec pour chaque mot la probabilité qu'il appartienne à un des différents _topics_. On donne un score _β_, qui est celui présenté infra.

```{r}
topics_2_Pro <- tidy(lda_2_Pro, matrix = "beta")
topics_2_Pro
```

```{r}
topics_3_Pro <- tidy(lda_3_Pro, matrix = "beta")
topics_3_Pro
```

### 2. Les paramètres de Gibbs
 
Les paramètres de Gibbs permettent une sophistication du système précédent. C'est une probabilité conditionnelle qui s'appuie, pour calculer le _β_ d'un mot, sur le _β_ des mots voisins. Pour ce faire, il faut déterminer:
1. À quel point un document aime un _topic_.
2. À quel point un _topic_ aime un mot.

#### 2. a) Installation de la library "ldatuning" pour déterminer le nombre optimal de topics
 
Pour installer cette library, il a été nécessaire de taper la commande suivante dans le terminal de l'ordinateur : sudo apt-get install libmpfr-dev (travail sous Linux).

```{r}
if(!require("ldatuning")){
  install.packages("ldatuning")
  library("ldatuning")
}
```

#### 2. b) Détermination du nombre optimal de topics.
##### 2. b) 1) Pour la Sylua
```{r}
#Exécution du calcul avec la fonction FindTopicsNumber
topicsNumber_Sylua <- FindTopicsNumber(
  #La DTM utilisée est la suivante :
  dtm_Sylua_clean,
  #Le nombre de possibilités testées :
  topics = seq(from = 2, to = 20, by = 1),
  #Les métriques utilisées
  metrics = c("Griffiths2004", "CaoJuan2009", "Arun2010", "Deveaud2014"),
  method = "Gibbs",
  control = list(seed = 77),
  verbose = TRUE #Si c'est FALSE, cela supprime tous les avertissments et les informations additionnelles.
)

#Utilisation de la fonction seq()qui permet de créer une séquence d'éléments dans un vecteur. La syntaxe est la suivante : seq (from, to, by, length.out) from = élément de début de la séquence ; to = élément de fin de la séquence ; by = différence entre les éléments ; length.out = longueur maximale du vecteur.

#Affichage du résultat
FindTopicsNumber_plot(topicsNumber_Sylua)
#Lecture du graph : "“Griffiths” et “Deveaud” suivent un principe de maximisation alors que “CaoJuan” et “Arun” obéissent à un principe de minimisation. Je vous épargne les détails techniques, mais l’idée ici est d’identifier l’endroit où simultanément “Griffiths” et “Deveaud” se rejoignent le plus et où c’est également le cas pour “CaoJuan” et “Arun”. Tout est histoire de compromis, trouver l’endroit ou l’écart entre les courbes est minimal en haut et en bas !" (source : https://ouvrir.passages.cnrs.fr/wp-content/uploads/2019/07/rapp_topicmodel.html)

#NB: En fait, il faut aussi regarder à quel moment la courbe stagne (surentraînement).
```
 Le nombre optimal de topics semble ici être 5 ou 6.
 
##### 2. b) 2) Pour le Pro Sacerdotum barbis
```{r}
#Exécution du calcul
topicsNumber_Pro <- FindTopicsNumber(
  #La DTM utilisée
  dtm_Pro_clean,
  #Le nombre de possibilités testées
  topics = seq(from = 2, to = 20, by = 1),
  #Les métriques utilisées
  metrics = c("Griffiths2004", "CaoJuan2009", "Arun2010", "Deveaud2014"),
  method = "Gibbs",
  control = list(seed = 77),
  verbose = TRUE
)
#Affichage du résultat
FindTopicsNumber_plot(topicsNumber_Pro)
```
 Le nombre optimal de topics semble être 6 (entre 5 et 7).
 
### 3. Exécution du calcul pour le topic modeling
##### 3. a) Pour la Sylua
```{r}
## Set parameters for Gibbs sampling
#Le modèle va tourner 2000 fois avant de commencer à enregistrer les résultats
burnin <- 2000
#Après cela il va encore tourner 2000 fois
iter <- 2000
# Il ne va enregistrer le résultat que toutes les 500 itérations
thin <- 500
#seed et nstart pour la reproductibilité
SEED=c(1, 2, 3, 4, 5)
seed <-SEED
nstart <- 5
#Seul le meilleur modèle est utilisé.
best <- TRUE
#7 topics
lda_gibbs_5_Sylua <- LDA(dtm_Sylua_clean, 5, method="Gibbs", control=list(nstart=nstart, seed=seed, best=best, burnin=burnin, iter=iter, thin=thin))
#Utilisation de la fonction LDA avec la dtm utilisée, le nombre de topics, la méthode et le contrôle appliqué.

#6 topics
lda_gibbs_6_Sylua <- LDA(dtm_Sylua_clean, 6, method="Gibbs", control=list(nstart=nstart, seed=seed, best=best, burnin=burnin, iter=iter, thin=thin))
```

On peut désormais voir les premiers résultats pour chacun des modèles. Il s'agit des mots dont la fréquence d'utilisation est corrélée.

```{r}
#"LDA 2"
#termsTopic_lda_2_Sylua <- as.data.frame(terms(lda_2_Sylua,10))
#head(termsTopic_lda_2_Sylua,11)
#"LDA 3"
#termsTopic_lda_3_Sylua <- as.data.frame(terms(lda_3_Sylua,10))
#head(termsTopic_lda_3_Sylua,11)
"LDA GIBBS 5"
termsTopic_lda_gibbs_5_Sylua <- as.data.frame(terms(lda_gibbs_5_Sylua,10))
head(termsTopic_lda_gibbs_7_Sylua,11)
"LDA GIBBS 6"
termsTopic_lda_gibbs_6_Sylua <- as.data.frame(terms(lda_gibbs_6_Sylua,10))
head(termsTopic_lda_gibbs_6_Sylua,11)
```

Nous allons utiliser `lda_gibbs_7_Sylua`, comme le nombre optimal de topics était de 7, et construire une matrice avec les _β_ des tokens (pour les ɣ, et donc des probabilités par document, on aurait mis `matrix = "gamma"`). Chaque token est répété deux fois, avec une probabilité pour chaque _topic_:

```{r}
topics_Sylua <- tidy(lda_gibbs_6_Sylua, matrix = "beta")
topics_Sylua
```

##### 3. b) Pour le Pro sacerdotum barbis
```{r}
## Set parameters for Gibbs sampling
#Le modèle va tourner 2000 fois avant de commencer à enregistrer les résultats
burnin <- 2000
#Après cela il va encore tourner 2000 fois
iter <- 2000
# Il ne va enregistrer le résultat que toutes les 500 itérations
thin <- 500
#seed et nstart pour la reproductibilité
SEED=c(1, 2, 3, 4, 5)
seed <-SEED
nstart <- 5
#Seul le meilleur modèle est utilisé.
best <- TRUE
#5 topics
lda_gibbs_5_Pro <- LDA(dtm_Pro_clean, 5, method="Gibbs", control=list(nstart=nstart, seed=seed, best=best, burnin=burnin, iter=iter, thin=thin))
#6 topics
lda_gibbs_6_Pro <- LDA(dtm_Pro_clean, 6, method="Gibbs", control=list(nstart=nstart, seed=seed, best=best, burnin=burnin, iter=iter, thin=thin))
```

On peut désormais voir les premiers résultats pour chacun des modèles. Il s'agit des mots dont la fréquence d'utilisation est corrélée.

```{r}
"LDA 2"
termsTopic_lda_2_Pro <- as.data.frame(terms(lda_2_Pro,10))
head(termsTopic_lda_2_Pro,11)
"LDA 3"
termsTopic_lda_3_Pro <- as.data.frame(terms(lda_3_Pro,10))
head(termsTopic_lda_3_Pro,11)
"LDA GIBBS 5"
termsTopic_lda_gibbs_5_Pro <- as.data.frame(terms(lda_gibbs_5_Pro,10))
head(termsTopic_lda_gibbs_5_Pro,11)
"LDA GIBBS 6"
termsTopic_lda_gibbs_6_Pro <- as.data.frame(terms(lda_gibbs_6_Pro,10))
head(termsTopic_lda_gibbs_6_Pro,11)
```

Nous allons utiliser `lda_gibbs_6_Pro`, comme 6 est apparu comme le nombre optimal possible de topics, et construire une matrice avec les _β_ des tokens (pour les ɣ, et donc des probabilités par document, on aurait mis `matrix = "gamma"`). Chaque token est répété deux fois, avec une probabilité pour chaque _topic_:

```{r}
topics_Pro <- tidy(lda_gibbs_6_Pro, matrix = "beta")
topics_Pro
```

#VI. Visualisation

## A. Récupération des mots

### 1. Installation de la library "dplyr"
Cette library facilite le traitement et la manipulation de données contenues dans une ou plusieurs tables en proposant une syntaxe sous forme de verbes.
```{r}
if (!require("dplyr")){
   install.packages("dplyr")
  library("dplyr")
}
```

### 2. Affichage des mots récupérés dans un graphe

#### 2. a) Pour la Sylua

Avec 6 topics

```{r}
#Recupération des mots
top_terms_Sylua <- topics_Sylua %>%
  group_by(topic) %>%
  top_n(10, beta) %>%
  ungroup()  %>%
  arrange(topic, -beta)
#Dessin du graphe
#On retrouve la fonction ggplot, cette fois-ci avec geom_col qui permet de créer des diagrammes à barres (barplots).
top_terms_Sylua %>%
  mutate(term = reorder_within(term, beta, topic)) %>%
  ggplot(aes(term, beta, fill = factor(topic))) + geom_col(show.legend = FALSE) +
                                                  facet_wrap(~ topic, scales = "free") +
                                                  coord_flip() +
                                                  scale_x_reordered()
```

#### 2. b) Pour le Pro Sacerdotum barbis

Avec 6 topics

```{r}
#Récupération des mots
top_terms_Pro_6 <- topics_Pro %>%
  group_by(topic) %>%
  top_n(10, beta) %>%
  ungroup()  %>%
  arrange(topic, -beta)
#Dessin du graphe
top_terms_Pro_6 %>%
  mutate(term = reorder_within(term, beta, topic)) %>%
  ggplot(aes(term, beta, fill = factor(topic))) + geom_col(show.legend = FALSE) +
                                                  facet_wrap(~ topic, scales = "free") +
                                                  coord_flip() +
                                                  scale_x_reordered()
```

## B. Association des tokens aux topics

Installation de la library reshape2 pour pouvoir utiliser la fonction melt qui permet de modifier le format des données en fonction d’une ou plusieurs variables de référence (passage d'une table large avec de nombreuses colonnes à une table haute avec de nombreuses lignes et peu de colonnes).

```{r}
if (!require("reshape2")){
  install.packages("reshape2")
  library("reshape2")
}
```
 
### 1. Pour la Sylua
```{r}
df_Sylua_2 <- melt(as.matrix(dtm_Sylua_clean))
df_Sylua_2 <- df_Sylua_2[df_Sylua_2$Terms %in%findFreqTerms(dtm_Sylua_clean, lowfreq = 50), ]
ggplot(df_Sylua_2, aes(as.factor(Docs), Terms, fill=log(value))) +
                                             geom_tile() +
                                             xlab("Sujets") +
                                             scale_fill_continuous(low="#FEE6CE", high="#E6550D") +
                                             theme(axis.text.x = element_text(angle=90, hjust=1))
```

```{r, fig.width=12, fig.height=12}
tt_Sylua <- posterior(lda_gibbs_7_Sylua)$terms
melted_Sylua = melt(tt_Sylua[,findFreqTerms(dtm_Sylua_clean, 50,500)])

colnames(melted_Sylua) <- c("Topics", "Terms", "value")
melted_Sylua$Topics <- as.factor(melted_Sylua$Topics)
ggplot(data = melted_Sylua, aes(x=Topics, y=Terms, fill=value)) +
                                                                      geom_tile() +
                                                             theme(text = element_text(size=35))
```

### 2. Pour le Pro Sacerdotum barbis
```{r}
df_Pro <- melt(as.matrix(dtm_Pro_clean))
df_Pro <- df_Pro[df_Pro$Terms %in%findFreqTerms(dtm_Pro_clean, lowfreq = 7), ]
ggplot(df_Pro, aes(as.factor(Docs), Terms, fill=log(value))) +
                                             geom_tile() +
                                             xlab("Sujets") +
                                             scale_fill_continuous(low="#FEE6CE", high="#E6550D") +
                                             theme(axis.text.x = element_text(angle=90, hjust=1))
```

Avec 6 topics
```{r, fig.width=12, fig.height=12}
tt_Pro <- posterior(lda_gibbs_6_Pro)$terms
melted_Pro = melt(tt_Pro[,findFreqTerms(dtm_Pro_clean, 10,20)])

colnames(melted_Pro) <- c("Topics", "Terms", "value")
melted_Pro$Topics <- as.factor(melted_Pro$Topics)
ggplot(data = melted_Pro, aes(x=Topics, y=Terms, fill=value)) + 
                                              geom_tile() +
                                              theme(text = element_text(size=35))
```

## C. Observation du _score gamma_
 
Le score gamma est la probabilité qu'un document contienne un sujet.

### 1. Pour la Sylua
```{r}
DocumentTopicProbabilities_Sylua <- as.data.frame(lda_gibbs_7_Sylua@gamma)
rownames(DocumentTopicProbabilities_Sylua) <- rownames(corpus_Sylua)
head(DocumentTopicProbabilities_Sylua)
```

### 2. Pour le Pro Sacerdotum barbis
```{r}
DocumentTopicProbabilities_Pro <- as.data.frame(lda_gibbs_6_Pro@gamma)
rownames(DocumentTopicProbabilities_Pro) <- rownames(corpus_Pro)
head(DocumentTopicProbabilities_Pro)
```

## D. Nuages de mots
 
Pour faire des faire des _word clouds_, il faut installer les libraries suivantes :
```{r}
if (!require("wordcloud")){
   install.packages("wordcloud")
  library("wordcloud")
}
if (!require("RColorBrewer")){
   install.packages("RColorBrewer")
  library("RColorBrewer")
}
if (!require("wordcloud2")){
   install.packages("wordcloud2")
  library("wordcloud2")
}
```

### 1. Pour la Sylua
On récupère les mots et on les associe à leur 𝛃

```{r, fig.width=20, fig.height=20}
tm_Sylua <- posterior(lda_gibbs_7_Sylua)$terms
data_Sylua = data.frame(colnames(tm_Sylua))
head(data_Sylua)
```


Puis on produit une visualisation par _topic_

```{r, fig.width=30, fig.height=20}
for(topic in seq(from = 1, to = 7, by = 1)){
    data_Sylua$topic <-tm_Sylua[topic,]
    #text(x=0.5, y=1, paste("V",topic, sep=""),cex=0.6)
    wordcloud(
      words = data_Sylua$colnames.tm_Sylua., #Mots à dessiner
      freq = data_Sylua$topic, #Fréquence des mots
      #Min.freq=sous ce seuil, les mots ne seront pas affichés
      min.freq=0.0002,
      #max.words=nombre maximum de mots à afficher
      max.words=20,
      #Random.order dessine les mots dans un ordre aléatoire. Si faux, ils sont dessinés par ordre décroissant de la fréquence.
      random.order=FALSE,
      #rot.per=% de mots à 90°
      rot.per=.35,
      #taille du graphe
      scale=c(10,10),
      #couleurs
      colors = brewer.pal(5, "Dark2")
      # il est possible de rentrer directement les couleurs qui nous intéressent
      #c("red", "blue", "yellow", "chartreuse", "cornflowerblue", "darkorange")
    )
}
```


### 2. Pour le Pro Sacerdotum barbis
On récupère les mots et on les associe à leur 𝛃. On s'appuie sur le nombre de 6 topics.

```{r, fig.width=20, fig.height=20}
tm_Pro <- posterior(lda_gibbs_6_Pro)$terms
data_Pro = data.frame(colnames(tm_Pro))
head(data_Pro)
```

Puis on produit une visualisation par _topic_

```{r, fig.width=30, fig.height=20}
for(topic in seq(from = 1, to = 6, by = 1)){
    data_Pro$topic <-tm_Pro[topic,]
    #text(x=0.5, y=1, paste("V",topic, sep=""),cex=0.6)
    wordcloud(
      words = data_Pro$colnames.tm_Pro.,
      freq = data_Pro$topic,
      #sous ce seuil, les mots ne seront pas affichés
      min.freq=0.0002,
      #nombre maximum de mots à afficher
      max.words=30,
      #Si faux, en ordre croissant
      random.order=FALSE,
      #% de mots à 90°
      rot.per=.35,
      #taille du graph
      scale=c(10,10),
      #couleurs
      colors = brewer.pal(5, "Dark2")
      # il est possible de rentrer directement les couleurs qui nous intéressent
      #c("red", "blue", "yellow", "chartreuse", "cornflowerblue", "darkorange")
    )
}
```

# VII. Sources

La grande majorité du code provient d'un cours de Simon Gabay (Université de Genève, 2022).
Les annotations et la structure du notebook viennent principalement d'Alice Leflaëc.

