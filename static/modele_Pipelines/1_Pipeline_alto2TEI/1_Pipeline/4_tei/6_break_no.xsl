<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <!-- attention à la balise hi entre chaque ligne de titre, il faut la retirer au préalable sinon le script ne fonctionne pas  -->
    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- quand la balise <lb> est précédée par une balise <reg> dans <choice> contenant ¬ 
         ATTENTION : ne fonctionne pas sur les changements de page, je ne l'ai pas inclu -->
    
    <xsl:template match="tei:lb">
        <xsl:variable name="precedingChoice" select="preceding-sibling::tei:choice[1]" />
        <xsl:choose>
            <xsl:when test="$precedingChoice and $precedingChoice/tei:reg[@type='ed' and contains(., '¬')]">
                <xsl:copy>
                    <xsl:apply-templates select="@*"/>
                    <xsl:attribute name="break">no</xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*"/>
                    <xsl:apply-templates/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
