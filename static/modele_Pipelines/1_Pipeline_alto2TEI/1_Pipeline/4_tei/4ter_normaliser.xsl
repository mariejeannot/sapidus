<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="reg/text()">
        <xsl:variable name="replacedText">
            <xsl:call-template name="replaceChars">
                <xsl:with-param name="text" select="."/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$replacedText"/>
    </xsl:template>
    
    <xsl:template name="replaceChars">
        <xsl:param name="text"/>
        <xsl:choose>
            <xsl:when test="contains($text, 'ſ')">
                <xsl:variable name="replaced" select="replace($text, 'ſ', 's')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ß')">
                <xsl:variable name="replaced" select="replace($text, 'ß', 'ss')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ij')">
                <xsl:variable name="replaced" select="replace($text, 'ij', 'ii')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ꝑiter')">
                <xsl:variable name="replaced" select="replace($text, 'ꝑiter', 'pariter')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ꝑ')">
                <xsl:variable name="replaced" select="replace($text, 'ꝑ', 'per')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ꝓ')">
                <xsl:variable name="replaced" select="replace($text, 'ꝓ', 'pro')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ꝗ')">q̃ 
                <xsl:variable name="replaced" select="replace($text, 'ꝗ', 'qui')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, ' q̃ ')"> 
                <xsl:variable name="replaced" select="replace($text, ' q̃ ', 'quæ')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'freq̃ns')"> 
                <xsl:variable name="replaced" select="replace($text, 'freq̃ns ', 'frequens')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, ' em̃ ')">
                <xsl:variable name="replaced" select="replace($text, ' em̃ ', ' enim ')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, ' tñ ')">
                <xsl:variable name="replaced" select="replace($text, ' tñ ', ' tamen ')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'Caleñ')">
                <xsl:variable name="replaced" select="replace($text, 'Caleñ', 'Calen[das]')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'p̃')">
                <xsl:variable name="replaced" select="replace($text, 'p̃', 'præ')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ꝰ')">
                <xsl:variable name="replaced" select="replace($text, 'ꝰ', 'us')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, '')">
                <xsl:variable name="replaced" select="replace($text, '', 'que')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, '')">
                <xsl:variable name="replaced" select="replace($text, '', 'quam')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 't́')">
                <xsl:variable name="replaced" select="replace($text, 't́', 'tur')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, '́')">
                <xsl:variable name="replaced" select="replace($text, '́', 'que')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ã')">
                <xsl:variable name="replaced" select="replace($text, '([tdc])ã', '$1an')"/>
                <xsl:choose>
                    <xsl:when test="contains($replaced, 'ã')">
                        <xsl:variable name="finalReplaced" select="replace($replaced, 'ã', 'am')"/>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$finalReplaced"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$replaced"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($text, 'ẽ')">
                <xsl:variable name="replaced" select="replace($text, '([tdc])ẽ', '$1en')"/>
                <xsl:choose>
                    <xsl:when test="contains($replaced, 'ẽ')">
                        <xsl:variable name="finalReplaced" select="replace($replaced, 'ẽ', 'em')"/>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$finalReplaced"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$replaced"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($text, 'ũ')">
                <xsl:variable name="replaced" select="replace($text, '([tdc])ũ', '$1un')"/>
                <xsl:choose>
                    <xsl:when test="contains($replaced, 'ũ')">
                        <xsl:variable name="finalReplaced" select="replace($replaced, 'ũ', 'um')"/>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$finalReplaced"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$replaced"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($text, 'õ')">
                <xsl:variable name="replaced" select="replace($text, '([tdc])õ', '$1on')"/>
                <xsl:choose>
                    <xsl:when test="contains($replaced, 'õ')">
                        <xsl:variable name="finalReplaced" select="replace($replaced, 'õ', 'om')"/>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$finalReplaced"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="replaceChars">
                            <xsl:with-param name="text" select="$replaced"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($text, 'm̃')">
                <xsl:variable name="replaced" select="replace($text, 'm̃', 'mm')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($text, 'ę')">
                <xsl:variable name="replaced" select="replace($text, 'ę', 'æ')"/>
                <xsl:call-template name="replaceChars">
                    <xsl:with-param name="text" select="$replaced"/>
                </xsl:call-template>
            </xsl:when>
            <!-- Ajoutez d'autres motifs et remplacements ici -->
            <xsl:otherwise>
                <xsl:value-of select="$text"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>