<?xml version="1.0" encoding="UTF-8"?>

<!-- Source XSL: Pauline Jacsont, adaptation 1: Elodie Paupe, adaptation 2 : Simon Gabay, adaptation 3 : Marie Jeannot-Tirole -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.loc.gov/standards/alto/ns-v4#" version="2.0"
    exclude-result-prefixes="xs">
    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"
        xpath-default-namespace="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"/>

    <xsl:strip-space elements="*"/>
    
    <!-- CHANGE VARIABLE -->

    <xsl:variable name="nma">https://api.digitale-sammlungen.de/iiif/image/v2/bsb11220144_00003</xsl:variable> <!-- Serveur iiif où j'ai pris le doc -->
    <!-- <xsl:variable name="library">12148</xsl:variable> 
    <xsl:variable name="document">1214324/</xsl:variable> -->
    <xsl:variable name="fileName">Epigrammta_1520</xsl:variable> 
    <xsl:variable name="xmlDocuments" select="collection(concat('../3_alto/fichiers_alto','/?select=?*.xml;recurse=yes'))"/>
   
   <!-- <xsl:variable name="xmlDocuments"
        select="collection('../alto/Tyard_1549_btv1b8617188p/?select=?*.xml;recurse=yes')"/> -->

    <xsl:template match="/" >
        <xsl:processing-instruction name="xml-model">
            <xsl:text>href="../ODD/ODD_Sapidus.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:text>
        </xsl:processing-instruction>
        <xsl:processing-instruction name="xml-model">
            <xsl:text>href="../ODD/ODD_Sapidus.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:text>
        </xsl:processing-instruction>
        <TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="In_Fabrum_1526">
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <title>Transcription des poèmes contre Faber qui auraient pu être composés par <persName xml:id="Sapidus">
                            <forename>Ioannes</forename>
                            <surname>Sapidus</surname>
                            <ptr type="isni" target="https://isni.org/isni/0000000072440292"/>
                        </persName>.</title>
                        <author xml:id="MJT">
                            <persName>
                                <forename>Marie</forename>
                                <surname>Jeannot-Tirole</surname>
                                <ptr type="orcid" target="0000-0002-6607-653X"/>
                            </persName>
                        </author>
                        <respStmt>
                            <resp>Transcription, segmentation</resp>
                            <persName ref="#MJT"/>
                        </respStmt>
                        <respStmt>
                            <resp>Scripts de conversion du format ALTO vers TEI</resp>
                            <persName xml:id="PJ">
                                <forename>Pauline</forename>
                                <surname>Jacsont</surname>
                                <ptr type="orcid" target="0000-0002-6296-3246"/>
                            </persName>
                            <persName xml:id="EP">
                                <forename>Elodie</forename>
                                <surname>Paupe</surname>
                            </persName>
                            <persName xml:id="SG">
                                <forename>Simon</forename>
                                <surname>Gabay</surname>
                                <ptr type="orcid" target="0000-0001-9094-4475"/>
                            </persName>
                            <persName ref="#MJT"/>
                        </respStmt>
                        <respStmt>
                            <resp>Scripts de conversion du format TEI vers txt et HTML</resp>
                            <persName ref="#MJT"/>
                        </respStmt>
                    </titleStmt>
                    <extent>
                        <measure unit="images" n="1"/>
                    </extent>
                    <publicationStmt>
                        <authority>Université de Strasbourg</authority>
                        <address>
                            <addrLine>14 rue René Descartes</addrLine>
                            <addrLine>67 084 Strasbourg</addrLine>
                            <addrLine>France</addrLine>
                        </address>
                        <availability status="restricted" n="CC-BY-NC-SA">
                            <licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/"/>
                            <p>Creative Commons Attribution 4.0 CC-BY-NC-SA 4.0</p>
                        </availability>
                        <date when="2023-11-14"/>
                    </publicationStmt>
                    <sourceDesc>
                        <bibl>
                            <title>Epistola V. Fabritii Capitonis, ad Huldericum Zuinglium quam ab Heluetiis forte interceptam, D. Ioan. Faber Constantiensis in Germanicum uersam deprauauit, una cum duabus Epistolis, quibus illum Concionatores Argentinenses ad collationem scripturarum prouocarunt</title>
                            <pubPlace>
                                <placeName cert="high" xml:id="Strasbourg">
                                    <settlement xml:lang="fr">Strasbourg</settlement>
                                    <ptr type="GeoNames" target="https://www.geonames.org/2973783/strasbourg.html"/>
                                </placeName>
                            </pubPlace>
                            <publisher>
                                <persName cert="high" xml:id="Köpfel" xml:lang="fr">
                                    <forename>Wolfgang</forename>
                                    <surname>Köpfel</surname>
                                    <ptr type="isni" target="https://isni.org/isni/0000000120184095"/>
                                </persName>
                            </publisher>
                            <date when="1526-08-12"/>
                        </bibl>
                    </sourceDesc>
                </fileDesc>
                <profileDesc>
                    <langUsage>
                        <language ident="fr">français</language>
                    </langUsage>
                    <textClass>
                        <keywords>
                            <term type="genre" n="poetry"/>
                            <term type="form" n="verse"/>
                            <term type="poet_type" n="collection"/>
                            <term type="related_person" n="Capito"/>
                            <term type="related_person" n="Faber"/>
                        </keywords>
                    </textClass>
                </profileDesc>
                <encodingDesc>
                    <classDecl>
                        <taxonomy xml:id="SegmOnto">
                            <bibl>
                                <title>SegmOnto</title>
                                <ptr target="https://github.com/segmonto"/>
                            </bibl>
                            <category xml:id="SegmOntoZones"/>
                            <category xml:id="SegmOntoLines"/>
                        </taxonomy>
                    </classDecl>
                </encodingDesc>
                <revisionDesc>
                    <change when="2023-10-11" who="#MJT">Conversion ALTO to TEI</change>
                </revisionDesc>
            </teiHeader>
            <sourceDoc xml:id="transcription">
                <xsl:for-each select="$xmlDocuments">
                    <xsl:for-each select="//alto">
                        <!-- Page -->
                        <!-- Select the page number -->
                        <xsl:variable name="page" select="substring-before(self::alto/Description/sourceImageInformation/fileName, '.')"/>
                        <xsl:element name="surface">
                            <!-- ID -->
                            <xsl:attribute name="xml:id">
                                <xsl:text>f</xsl:text>
                                <xsl:value-of select="$page"/>
                            </xsl:attribute>
                            <!-- Coordinates -->
                            <xsl:attribute name="ulx">
                                <xsl:value-of select="//Page/PrintSpace/@HPOS"/>
                            </xsl:attribute>
                            <xsl:attribute name="uly">
                                <xsl:value-of select="//Page/PrintSpace/@VPOS"/>
                            </xsl:attribute>
                            <xsl:attribute name="lrx">
                                <xsl:value-of select="//Page/PrintSpace/@WIDTH"/>
                            </xsl:attribute>
                            <xsl:attribute name="lry">
                                <xsl:value-of select="//Page/PrintSpace/@HEIGHT"/>
                            </xsl:attribute>
                            <!-- Add image's ark -->
                            <xsl:element name="graphic">
                                <xsl:attribute name="url">
                                    <xsl:value-of select="concat($nma,'/full/full/0/default.jpg')"/> <!-- à modifier -->
                                </xsl:attribute>
                            </xsl:element>
                            <!-- Regions -->
                            <xsl:for-each select="//TextBlock">
                                <xsl:element name="zone">
                                    <xsl:attribute name="xml:id">
                                        <xsl:text>f</xsl:text><xsl:value-of select="$page"/><xsl:text>_</xsl:text><xsl:value-of select="@ID"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="type">
                                        <!-- Attraper la valeur codée -->
                                        <xsl:variable name="type_zone">
                                            <xsl:value-of select="@TAGREFS"/>
                                        </xsl:variable>
                                        <!-- Recherche de la véritable valeur exprimée -->
                                        <xsl:variable name="type_zone_valeur">
                                            <xsl:value-of
                                                select="//OtherTag[@ID = $type_zone]/@LABEL"/>
                                        </xsl:variable>
                                        <xsl:value-of select="$type_zone_valeur"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="n">
                                        <xsl:number level="single" count="." format="1"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="ulx">
                                        <xsl:value-of select="@HPOS"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="uly">
                                        <xsl:value-of select="@VPOS"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="lrx">
                                        <xsl:value-of select="@WIDTH"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="lry">
                                        <xsl:value-of select="@HEIGHT"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="points">
                                        <xsl:variable name="value" select="./Shape/Polygon/@POINTS"/>
                                        <xsl:analyze-string select="$value"
                                            regex="([0-9]+)\s([0-9]+)">
                                            <xsl:matching-substring>
                                                <xsl:for-each select="$value">
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  <xsl:text>,</xsl:text>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  <xsl:text> </xsl:text>
                                                </xsl:for-each>
                                            </xsl:matching-substring>
                                        </xsl:analyze-string>
                                    </xsl:attribute>
                                    <xsl:attribute name="source">
                                        <xsl:value-of select="$nma"/>
                                        <xsl:text>/</xsl:text>
                                        <xsl:value-of select="@HPOS"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="@VPOS"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="@WIDTH"/>
                                        <xsl:text>,</xsl:text>
                                        <xsl:value-of select="@HEIGHT"/>
                                        <xsl:text>/full/0/default.jpg</xsl:text>
                                    </xsl:attribute>
                                    <!-- Ajouter les lignes -->
                                    <xsl:for-each select="TextLine">
                                        <xsl:element name="zone">
                                            <xsl:attribute name="xml:id">
                                                <xsl:text>f</xsl:text><xsl:value-of select="$page"/><xsl:text>_</xsl:text><xsl:value-of select="@ID"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:text>segmontoLine</xsl:text>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <!-- Attraper la voiture -->
                                                <xsl:variable name="type_zone">
                                                  <xsl:value-of select="@TAGREFS"/>
                                                </xsl:variable>
                                                <!-- Recherche de la véritable valeur -->
                                                <xsl:variable name="type_zone_valeur">
                                                  <xsl:value-of
                                                  select="//OtherTag[@ID = $type_zone]/@LABEL"/>
                                                </xsl:variable>
                                                <xsl:value-of select="$type_zone_valeur"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="n">
                                                <xsl:number level="single" count="." format="1"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="points">
                                                <xsl:variable name="value"
                                                  select="./Shape/Polygon/@POINTS"/>
                                                <xsl:analyze-string select="$value"
                                                  regex="([0-9]+)\s([0-9]+)">
                                                  <xsl:matching-substring>
                                                  <xsl:for-each select="$value">
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  <xsl:text>,</xsl:text>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  <xsl:text> </xsl:text>
                                                  </xsl:for-each>
                                                  </xsl:matching-substring>
                                                </xsl:analyze-string>
                                            </xsl:attribute>
                                            <xsl:attribute name="source">
                                                <xsl:value-of select="concat($nma,'/')"/> <!-- à modifier-->
                                                <xsl:value-of select="@HPOS"/>
                                                <xsl:text>,</xsl:text>
                                                <xsl:value-of select="@VPOS"/>
                                                <xsl:text>,</xsl:text>
                                                <xsl:value-of select="@WIDTH"/>
                                                <xsl:text>,</xsl:text>
                                                <xsl:value-of select="@HEIGHT"/>
                                                <xsl:text>/full/0/default.jpg</xsl:text>
                                            </xsl:attribute>
                                            <!-- Baseline -->
                                            <xsl:element name="path">
                                                <xsl:variable name="nbaseline">
                                                  <xsl:number level="single" count="." format="1"/>
                                                </xsl:variable>
                                                <xsl:attribute name="n">
                                                  <xsl:value-of select="$nbaseline"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="xml:id">
                                                  <xsl:value-of
                                                      select="concat('f',$page,'_',@ID, '_baseline_', $nbaseline)"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="type">
                                                  <xsl:text>baseline</xsl:text>
                                                </xsl:attribute>
                                                <xsl:attribute name="points">
                                                  <xsl:variable name="value" select="@BASELINE"/>
                                                  <xsl:analyze-string select="$value"
                                                  regex="([0-9]+)\s([0-9]+)">
                                                  <xsl:matching-substring>
                                                  <xsl:for-each select="$value">
                                                  <xsl:value-of select="regex-group(1)"/>
                                                  <xsl:text>,</xsl:text>
                                                  <xsl:value-of select="regex-group(2)"/>
                                                  <xsl:text> </xsl:text>
                                                  </xsl:for-each>
                                                  </xsl:matching-substring>
                                                  </xsl:analyze-string>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <!-- Ajouter la transcription -->
                                            <xsl:element name="line">
                                                <xsl:variable name="nline">
                                                  <xsl:number level="single" count="." format="1"/>
                                                </xsl:variable>
                                                <xsl:attribute name="xml:id">
                                                  <xsl:value-of
                                                      select="concat('f',$page,'_',@ID, '', $nline)"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="String/@CONTENT"/>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:for-each>
            </sourceDoc>
            <xsl:element name="text">
                <front>
                    <div xml:id="intro">
                        <head>Introduction</head>
                        <p xml:id="intro_p1">Nous avons intégré une version régularisée du texte dans la balise &lt;reg type="norm"&gt; . Nous avons effectué les changements suivants :
                            <list>
                                <item>ſ en s</item>
                                <item>ß en ss</item>
                                <item>ij en ii</item>
                                <item>́ en que (avec disparition de l'accent<note anchored="true">Il aurait été trop complexe de réintégrer l'accent sur la voyelle de la syllabe précédente, comme attendu.</note>)</item>
                                <item> en que</item>
                                <item> en quam</item>
                                <item>t́ en tur</item>
                                <item>ꝑ en per</item>
                                <item>ꝓ en pro</item>
                                <item>ꝰ en us</item>
                                <item>ꝙ en quod.</item>
                            </list>Nous avons également développé les nasales marquées par un tilde (ũ, ẽ, õ, ã, m̃, ñ) sans le mentionner.</p>
                    </div>
                    <div xml:id="chaîne_de_traitement">
                        <head>Chaîne de traitement</head>
                        <p>La transcription a été réalisée avec le moteur HTR <ref target="https://github.com/mittagessen/kraken">Kraken</ref> et le modèle HTR Gallicorpora+ du projet <ref target="https://github.com/Gallicorpora">Gallicorpora</ref>. Les fichiers ALTO de transcription de chaque page sont en ligne sur mon dépôt GitHub <ref target="https://github.com/Johann-Sapidus-1490-1561/HTR/tree/main/data/works/Apologia_1534">Data repository for HTR model training</ref>. Nous avons utilisé des <ref target="https://git.unistra.fr/mariejeannot/sapidus/-/tree/master/static/modele_pipeline/1_Pipeline_alto2tei">scripts XSLT</ref> pour transformer les fichiers en ALTO en TEI, puis pour enrichir les données. Une fois le fichier XML achevé, nous avons utilisé une seconde <ref target="https://git.unistra.fr/mariejeannot/sapidus/-/tree/master/static/modele_pipeline/2_Pipeline_export">pipeline avec des scripts XSLT</ref> pour effectuer les exports en txt, html et tex. La scansion des poèmes a été ajoutée grâce à une troisième <ref target="https://git.unistra.fr/mariejeannot/sapidus/-/tree/master/static/modele_pipeline/3_Pipeline_scansion">pipeline</ref>.</p>
                    </div>
                </front>
                <xsl:element name="body">
                    <xsl:element name="p"/>
                </xsl:element>
            </xsl:element>
        </TEI>
    </xsl:template>

</xsl:stylesheet>
