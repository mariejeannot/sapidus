<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    
    <!-- Correspondance sur le corps du texte -->
    <xsl:template match="body">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Correspondance sur chaque ligne -->
    <xsl:template match="body//l">
        <xsl:variable name="sectionNum" select="count(preceding::text()[contains(., '$%')]) + 1"/>
        <xsl:variable name="poemNum" select="count(preceding::text()[contains(., '%$')]) + 1"/>
        <xsl:variable name="verseNum" select="count(preceding::l) + 1"/>
        <!-- Ajout du numéro de section dans l'attribut @xml:id -->
        <xsl:attribute name="xml:id">
            <xsl:text>s</xsl:text>
            <xsl:value-of select="$sectionNum"/>
            <xsl:text>p</xsl:text>
            <xsl:value-of select="$poemNum"/>
            <xsl:text>v</xsl:text>
            <xsl:value-of select="$verseNum"/>
        </xsl:attribute>
        <!-- Ajout du numéro de vers dans l'attribut @n -->
        <xsl:attribute name="n">
            <xsl:value-of select="$verseNum"/>
        </xsl:attribute>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
    
    <!-- Modèle de correspondance par défaut pour copier les autres éléments tels quels -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
