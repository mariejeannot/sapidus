IOAN. SAPIDI EPITAPHIA:
SIVEGY MNASII ARgentoratensis luctus.
Argentoratiper Vuendelinum Rihelium.
IOANNIS SAPIDI, AE
GLOOA. BATRACHVS. QVAR
est de morte Francisci Froschii lurisconsulti & Eobani
Hessi Poetæ.
Opulea reliquis ouibus sub fronde relictis,
Vnam, quàm male perdideram, uagus omnibus
aruis
Querebam, tum forte cauum lustrare uolebam
Antrum, non procul à Rhenane flumine ripe,
Quod tegit innumeris hederacea scena corymbis.
Hic ego percipio mixtos cum murmure luctus:
Accedo propius, uideo intro, conspicor intus
Pastores, nec eis uideor, me subtraho rursum.
Post truncumsalicis, antro incumbentis eidem,
Auribus hic adsto cupidis, & erant duo, nempe
Diuinus Melibœus & ingeniosus Iolas,
Deflebantq ibi uita exutos nuper amicos
Ambos pastores, hic Thyrsida, Batrachon illeEt cæpit prior his Melibœus uocibus uti.
Batrache tune iaces, quo non prastantior ullus
Certas pascendi pecoris prescribere fines.
Pascua que sint armentis, quæ grata capellis,
Que que ouium dulci distentent ubera lacte:
Defessum me colloquio recreabit amico,
Quis mihi dulcisonos cantus resonabit ad aures
Quis tumidis cannas buccis inflabit agrestes.
Quisue lyram subitis digitis puisabit acutam,
Horum etenim fucras mi Batrache tam bene doctus
Quamuix ullum alium pastorum uidimus unquam
Nunc has in te delicias amisimus omnes,
In quarum mors dira locum succedere fecit
Singultus lachrymas, planctus, suspiria, luctus,
Quæ mala non, nisi suprema cum luce, recedent.
Nunc quoniam re non aliami Batracheuiuus
Vtilis esse potest defunco munereuite,
R estat ut incidi faciam memorabile carmen,
Carmen te dignum, gelidodignumque sepulchro,
Quodque tui longam pastorum posteritatem
Admoneat, sed me quia non mediocriter alto
Fixum corde premituulnus, quod meque potentem
Nonsinit esse mei, & uires mihi mentis ademit.
Vt nequeam quod percupio componere carmen.
Inse suscipiet meus Alphesibœus idipsum
Muneris, hunc ego, quàm primum conuenero, multis
Vrgebo precibus, detestaborque per omnes
Pastorum leges & pastoralia iura,
Que melius nemo nouit te Batrache quondam,
Donec is ediderit (magnopermotus & illo
Quo me te y fuit semper complexus amore)
IOAN. SRPIDI
Carmen & in laudem tibi & in solatia nobis:
Hæc est singultim Melibœus uerba locutus.
Nunc recita mihi, Musa, fuit que questus Iolas:
Ah quis fons tantam uim suppeditabit aquarum.
Materiamqs meis oculis infusus abundam
Sufficiet, lachrimas madefacta per oraruentes,
Vt noctesq̃s diesq̃ meum deflere per omnes
Thyrsida sat possim, quem mors indigna peremit,
Mors nimium crudele & inexorabile fatum.
Me miserum miserum iaceat Germanicus Orpheus
Qui iuga Meliboci & syluas & flumina & arua
Et tecta & pecus & pecoris persæ pe magistros
Leniit argutæ dulcis modulator auenæ.
Qui Siculos agnos latiales duxit in agros,
Atque gregem iliacum ac armenta coegit eodem,
Quiq; Palestini pastoris carmina sacra,
Carminasydereo quondam de uertice fusa,
Ausus erat simul Ausonia resonare cicuta.
Et qui præeterea uersus & lusit & ultra
Ludere decrerat, nam præter cetera Thyrsi
Fastos pastorali annalia sacra quieti
Debita iam meditabaris, sed ferrea fati
'mperia inuisi cœptos risere labores,
Irrita sæceruntque pii conaminauoti
O Thyrsi o animi pars inseparabilis huius,
Doncc ego his oculis uiuus mea septauideb
Namg tibi laudem peperisset isunus, Athænis
Et Rome multi quam peperere uiri.
Tumulus Nicolai Gerbelii lunioris.
Nicolaos obiit mortem Gerbellius, annos
Vix circiter natus quater tres & duos.
Hunc Deus ornarat large tot dotibus unum,
Quas parcius dare singulas multis solet.
Visa fuit sors hæc tetricis nimis improba Parcis
Vitamques puero sustulere floridam.
Mortuus est ætate puer, sed mortuus idem
Plusq̃ senex animi bonis tot inclytis.
Ergo diu uixit satis & pariter bene uixit
Quoniam puer peruenit ad frugem senis.
Tumulus Caspari & Iosephi
Hedionum Fratrum.
Corput Caspari iacet hic Hedionis humatum,
Cum fratre Iosephosuo:
Spem fuit eximiam de se dare uisusuterque,
Sed trux eam mors abstulit
Namque bonos tenera pueros ætate peremit,
Multum tamenuite fuit,
Quando preclarum specimen puerilibus annis
Dedere adulti temporis.
Tumulus Ioslæe Mengii.
Heu Mengi quam funus adolescentis acerbum,
Namuir tulisset omne punctum in omnibus.
Tumulus Eusebii Oecolampadii.
Cui datur à splendore domus cognomen auitum
Eusebius occidit puer.
Tam non ouum ouo simile est, quam rettulit ille
Corpore animoque suum patrem.
Hinc nati spe Palladius quanta excidit ordo
Patet ex parentis fructibus.
Spem raro euentus sequitur, sed sæpe timori
Vsu uenit, quod præuidet.
Tumulus Guilbelini Zuingli.
Optimaquæque rapit diræ uiolentia mortis
Hæc superesse eadem pessima queque sinit.
Vtliquet, ante alios, Guillelmi in funere Zuingli
Non plene nati quinque; trieterides,
Cui, mirum dictu, noua cœmiteria primo
Hac seruant gelidaflebile corpus humo.
Nobilior patria fuerat uirtute futurus
Quippe puer iam par cœperat esse seni.
Hoc si uicturo respublicaleta fuisset,
Debet & extincti publicus esse dolor.
Tumulus Saræ Buceræ Martini Bucerifiliæ.
Saraiaces sacro mihi gurgite filia facta,
Patreque nata probo, matreq Saraiaces.
Outinam mihiius fati, aut tu mortua nunq̃
Aut esses dono iam rediuiua meo.
Hoc poterant animi dotes, argutia forme,
Et clari mores promeruisse tui.
Heu qualis fueras olim matrona futura
Cum tot eras diues uirgo tenella bonis
Nunc quoniam fatum immotum fixumque, sequemur
Nobis præmissam te, mea Sarauale.
Tumulus Sebastiani Sophæri
Geruasii Sophærifilii.
Siqui parentes debuere luctibus
Lætum suorum liberorum prosequi,
Sebastiano iure Suphæro, pater
Materg mœsui id exequuntur muneris.
Nam singularem præter elegantiam
Morum atque literarum, ea obseruantia
Illos coluit, adhuc adulescentior,
Vt si necessitas tulisset, tam parens
Quam filius fuisset, alitarum piæ
Ciconiarum amans uicissitudinis.
An non parentes iure luctui uacant
Quando duos lugent, in uno, mortuos?
IOAN. SAPIDI
Tumulus Ioannis Baprista
Ruelli.
Expirauit Ioannes Baptista Ruellus
Doctus adulescens integer, atque probus
Spem magnam patrio de sese ostenderat orbi
Sed mors ostensam noluit esse ratam
Nam qui dignus erat Pilie par esse senectæ
Non annos potuit bis numerare nouem
Spes licet immodicum faciat præcisa dolorem
Grata tamen casse mentio facta rei est.
Tumulus Iacobo Bedroto Sacer.
Ergo iacet uirtus Iacobi magna Bedroti,
Quam potuit nullus debilitare labor:
Seu populus sacra fuit instituendus in æde,
Seu pubes musis erudienda bonis,
Seu quam rem frugi consulta roganda senatus,
Seu qua in conuentu discutienda graui,
Siue elementa scolæ melius formanda recentis,
Seu uia successus continuanda probl.
Seu pro pauperibus suips comportanda iuuandis.
Seu mens erroris lapsa monenda mali,
Seu miser agrotus uerbis solandus amicis,
Seu pia quæque alio res facienda modo,
Hic fieri summa uir sedulitate sategit:
Vuum tan multis quis satis esse putets
Non qquidem tantum(ifame credimus) Atlas
Sudauit, tergo cum tulit astra suo,
Tanta nec Alcide moles, cumgrandia monstra
Susinuit forti perdomuitque manu.
Hic sua postponens alienis commoda rebus,
Se minime natum nouerat esse sibi,
Huc studia, huc artes, huc linguas, denique uertit
Huc oneris quicquid nocte dieque tulit.
En fructus quos illa bonos bona protulit arbor
Ex qua plantari debuit omne solum.
O bona mors hominis, quidesidis otiauite
Odit & innumeris gratificatus obit.
Hunc tumulum quem sanus adhuc Bedrote petebas
Ante dies septem, quam morereris, habe.
Seu casu, seu prasagasis mentelocutus,
Qualis agit magnos uis aliquando uiros,
Cum risu tamen officium tunc forte receptum,
Cum fletu Sapidus nunc egosoluo tibi.
Tumulus Elizabethæ filiæ Mar.
tini Buceri.
Heus etiam natamrapit implacabile fatum
In qua mater adhuc uirgine uiua fuit.
Namseu formam animi spectes seu corporis, unam
Dicu ex omni parte fuisie duas:
Quin iures ex se matremgenuisse seipsam
Atque adeo dupliciuitam habuisse modo
Non igitur (dictu res mira ) negauerit ullus
In nata matrem rursum obiisse sua.
TumulusAlexandri Itale
Theologi.
Hacrequiescit Alexander tellure, supremi.
Expectans tempus Iudiciale fori
Et simul impleri numerum sortemque piorum
Vniriq animis membra resumpta suis
Annon expectet nunc immortalis idipsum
Quod mortem cupiit tantopere ante suam.
Hinc qualis fuerit non commemorare necesse est,
Is fælix cui par illius esse datur.
Pancratii Benefictoris Poloni.
Pancratius Benefactoricui nomen auitum
Magna Cracouinæ spesque decusque plage
Vt uarios hominum ritus moresque receptos
Disceret in uariis perspiceretque locis
Et se Mufarum studiis ornaret amœnis
Nondum, Sarmaticos liquit, ephœ bus, agros,
Atque peragrauit Germana per opida late
Traiectoque adist regna Britanna salo
Ruraque Galliacæ uidit fœlicia terræ
Nota& huie, quanta est, italis ora fuit.
Tandem posthabita patriæ dulcedine, sedem
Argentorati legit habere suam
Hicque sibi peregre suscepti ferre laboris
Prouentus populo percipiente sacros
Sed mors egregium iuuenem in conamine primo
Abstulit, heu nimium mors inimica bonis:
Ergo, salutatis, nemo nisi manibus istis
Hacchonor extinctis siquis habendus)eat.
Tumulus lacobi Villici.
Iacobus hoc inest sepulchro villicus
Iuuenis ad omnes res gerendas gnauiter
Appositus & promptus, supra omnium fidem:
Hinc, non secus quam Cæcias uim nubium,
Magnesue massas ferreas ad se trahit,
Is beneuolentiam omnium in se transtulit,
Quiscunq gustandum semel se præbuit.
Nunc mortuus dolore multos afficit
Sed iure multorum graui cum luctuobit
Quem non sine ratione multi diligunt.
Tumulus Helenæ uxoris Pane
talconis Blasii.
Quæ iacet hic Helenæ quo tempore uirgo mancbat
Visa est uirginei gloria prima chori
Fostquam, legitimi, nupsit, matura mariti,
Non illi, é cunctis par fuit ulla nurus.
Inter item matres olim regnasset honestas
Seu proles, seu res instituenda domus.
Si non coniugii in foribus defuncta fuisset
Vita annos nondum bis numerante nouem.
Nemo bonam bonus hanc debet deflere puellam
Defleri debct quæ mala cumque perit.
Tumulus Filiolo Martini
Buceri Sacer.
Hic infans patrem referebat nomine & ore
Forterelaturus sic quoq mente fuit
Nam pueri, uix bimatum, uis insita, nati
Cœperat æquandos anteuenire dies.
Id doluit Parcis & mox in limineuitæ
Extinctos artus hocposuere loco.
Attamen insonti bona mors fuit ista puello,
Quamlibet est multis plus mala uisa satis.
Nam qui non meruit, timuit, uel obire cupiuit
Annon ille sua morte beatus obit.
Tumulus Conradi Molsemi Pueri.
Conradus hoc Molsemus in solo iacet.
Quo uix puer fuit allus nobilior
Fœlicitate morum & oris & ingeni.
Sed raritas rei decoræ, ut non parum
Difficilis est, ita cito plerumque interit.
Tumulus Ioannis Isenburgii Praæceptoris classici.
Tectus Ioannes tumulo isenburgiusisto
Formator optimus rudis pueritie
Phidiaci nunquam signa expressere labores
His lineamentis & hac argutia,
Quàm fuit informes animos hic arte magister
Doctus polire literis & moribus.
Omnia mors aufert & ius quoques prerogat istud
Vt quemque frugi hominem sibi cito uendicet.
Tumulus Vuolphgangi Capitonis.
Vuolphgangus Capito mortalia membra solutus
Cœlestis subiit regna beata soli
Quo prius & uita & sermone uocauerat agnos
Ille bonus pastor, nunc ibi iure manet.
Dignus erat maiori æuo, sed non fuit ultra
Tam charum Mundus dignus habere caput
Ast habet: hic sua iacturæ solatiafactæe,
illuus adflatu scripta referta sacro.
Quæ qui legerit, inque animum demiserit altum
Diuint consors hie Capitonis erit.
Tumulus Danielis Miægi
uiri consularis.
Daniel Miægus peste correptus graui
Grauiore luctugentis amissus suæ
Reiques publicæ malo grauissimo
Desyderatus, at suo tamen ipsius
Compendio mortalis esse desit.
Tumulus Hierommi lupi
Procuratoris.
Iacet Hieronymus Lupus uir optimus
Ad rem familiarem ministrandam optime,
Defunctus est non absque fletu plurium,
Dignus quidem quem ciuis omnis defleat,
Si luctibus reparabilis casus foret.
Tumulus Elizabethæ Vxoris
Martini Buceri.
Elisabetha inest hac humo uxor optima
Viri sacrarum literarum principis
Permagna fœminei ordinis laus, utpote
Imitabile exemplum gubernande domus
Et norma connubii colendi, ita suauiter
Pieque uixit cum suo domino, sed hanc
Mors non tulit fæ licitatem coniugun
Diuortioquẽ separauit perpeti.
Quas res secundas, uel aliorum casuum
Tutas ab impetu, illa non tandem opprimit.
T'umulus Ionæ, lacobi
Bedroti filii.
Tam cito ne Ionam mors Bedrotum sustulit ?
Tot præditum uel in ipsa adulescentia,
Præter decorem corporis, animi bonis?
Quorum unumquodqs uirum beatum redderet,
Que si spei fructum dedissent debitum,
In rem rei ualde fuissent publicam,
Ipsumque fortuitis beassent dotibus.
Sed quando fatorum his tenemur legibus,
Vt simus id, non quod lubet, sed quodlicet,
Feramus, at nobis sit hoc solatio,
Spem destututam commodo lædere minus,
Quam si, metu quod torserat, malum ingruat.
T’umulus Hieronymi Emmeri
Argen. Adulescentis.
Non Hieronymus Emmerus debebat adusg
Tempora maturi iusta uenire uiri:
Sed ueluti uernis plantaria floribus arbos
Eruitur ualido tota reuulsa Noto.
Sic is adulescens pereat florentibus annis,
Qui spes summe ingens utilitatis erat.
Vos sacra Musarum qui discitis atque docetis,
Hoc funus fletu triste rigate pio.
Tumulus Fœlicitatis Filiolæ
Martini Buceri.
Fœlicitas, ut cito foret, quod dicitur,
Cito moritur, nam cito mori fœlicium est.
Proinde nemo, quamlibet uel corpore,
Vel sorte, uel animo uidetur præpotens
Fœlix prius putandus est quam mortuus:
Quando, quod instabile, beatum non facit
Sed miserum, at ipsa mors miseriæ finis est
Et sola fons fœlicitatis, ergo qui
Quo citius efflarit animam, hoc citius quoq
Fit particeps fœlicitatis perpetis,
Qua nunc fruitur, hic que iacet, fœlicitas.
Tumulus Annæ Miægæ Danielis
Miægi Filiæ.
Annam Micgam tumulus iste contegit,
Illm iuuandis indigis plenam manum:
Rarum & pudicitie decus, quam mortuo
Cui uirgo nupserat, sacrauit coniugi,
Nolens secundas experiri nuptias
Nouoque amore pristini uim tollere:
Quin maluit semel uirum amissum probum
Deflere, quam uel rursus amittere parem,
Vel ferre, non assueta, mores improbi.
Sic finis idem luctui & uite fuit.
Iam, testimonio simul laudabili,
Studio simul paria æmulandi, castitas
Et liberalitas modos hos dedicant.
Tumulus Vuolphgangi Capitonis
Iunioris infantis.
Ceu nouus è ueteri Phœnice, renascitur idem,
Sic & in hac Capito prole renatus erat.
Vt quæ tam plane similis fuit omnia patri
Adde opera atg annos idem erit ipse pater
Hinc sperabatur decus hoc insigne futura
Progenies genitor quod fuit atque manet
Spes dubia & fallax, nõ paruum sæpe dolorene
Datuice successus irrita facta sui.
Tumulus Annæ Regiæ, uxoris Sebaldi Hauuenreuteri Norici, Medici.
Heus subsiste parum qui transis, nosque uiator
Versus(nam temere hac non licet ire)lege.
Quæ iacet hic, poterat non paucis una puellis
Vel maior uel par dotibus esse suis.
Tumulus Chrysostomi. Hedionis fili
Puer loquitur.
Debet & hocfactum nulla ignorareuetuslas
Obliuionis & omnis esse liberum.
Tumulus D. Vuendelini Bitelbrunii.
Vuendlinus hac iacet Bitelbrunnius humo,
Iuris magister optimus scientiæ,
Fidissimusq cognitor clientibus,
Idemq longe Musicus dulcissimus,
Pius in Deum, natos, parentes, coniugem,
Patriamques & in amicis colendis sedulus,
Et commodus semper iocis & seriis.
Quem, quoniam ei talem esse non licuit diu,
Testetur hæec talem fuisse inscriptio.
Tumulus Symonis Grynæi. Interlocutores Viator & Cyclopædia.
Via. Quem uideo? certe fœmina est, mœret, adeo cam,
Salue. Cy. o, quod optas ore, si mihire dares:
Via. Quid ita? Cy.uirum, qui familia tota mea
Sic familiariter usus est, ut haud magis
Quisquam alius, ex quo nata fuit, amisi, & in
Eo salutem omnem meam. Via, quod nomen est?
Cy. Mihiné? Vi tibi.C. Cyclopædie. Vi.ille qreC. Symõ
Gryneus. Via, at quamnam familiam ais tuam?
Cy. Grex disciplinarum omnium paret mihi.
Via. Noui puellas, familiam certe inclytam.
Sed coluit omnes unus ille. Cy, omnes quidem.
Via. O delicium, is unus tot inter amiculas.
Quam lusitauit suauiter. Cy, paucis habe.
Cum primum adhuc puer in meum cœtum fuit
Admissus, ilico meæeuisa indole
Tam nobili, cæpere amore prosequi,
Vt quibus erat persuasum amatum eri simul
Se mutuo à puero, ac eas plane nihil
Opinio fefellit, is enim cupidissime
Complexus est omnes, animo & oculis ferens,
Primum tamen ad aptas suo æuo & captui
Adiecit animum, à quibus, ut infimo gradu,
Seueriarum ad studia paulatim appulit
Et omnibus se ita docilem & gratum obtulit
Vt intimis penetralibus recæperint
Prorsusque rerum eum nibhil celauerint
Sic ut iuuenis adhuc nihil non didicerit.
Via. Proinde tres artes loquendi primitus
Coluit. Cy. ita est. sed breuiter audi singula
Decreta Grammatices nihil nouit minus
Quam Priscianus, tam prope eloquentiæ
Ciceroniane accessit, ut nemo propius,
Crysippon exequauit in dialecticis,
Non hoc lopas melius increpuit lyram,
Numeriscientia zoy èvroy rettulit,
Talis Geometer, qualis Archytas fuit,
Cognitionem habuit, ut Atlas, syderum,
De moribus consimili Aristoteli modo
Disseruit & sapientia cœlestium,
Non Plinio physices priores detulit,
Non cessit Hippocrati medendi indagine
Fuit alter Aurelius sacris in literis,
Iam quid sacre non attigit Poæseos?
Quid est relatu dignum in historiis nouls
Et ueteribus, simulque miscellaneis
In lucubrationibus, quod nescut?
Linguis Palestina, Latina & Attica
Non alter ac sermone plebeio usus est.
Sed quid opus est multis, fuit plane Hippias.
Aut alter aut uel præferendus Hippiæ.
Via. Magnum mihi narras uirum. Cy. uere tamen:
Via. Sed quid tibi hic negocii est? Cy tumulum colo.
Via. Puto tibi Grynei tui tumulum sacrum,
Vt Apollini huius nominis suum nemus.
Cy. Recte putas, prius etenim mortalitas
Habitura ius mei meæque familie
Quam deseram hunc locum, uolo hoc diutius
Lugere mortuum meuum, quo gaudium
Exuiuo eo mihi miseræ breuius fuit.
Via. Miseret tui me. Cly iure miseret te mei
Sed nunc abi, colloquii & huius sis memor.
Via. Quo ad uixero, fuere memoro, uale. Cy. uales.
Tumulus D. Fridrichi Comitis a Beichlingé Primarũ Argent & Collos
Ecquis adhuc æquam & mortem non dicat iniquam,
niens. Ecclesiarum decani.
Que tollit cito quos æquum erat esse diu.
Quando tam cito Fridrichus Beichlingius, illud
Grande decus patriæ nobilitatis, obit?
Quo uiuo illius clarissima gloria gentis
Non uno fuerat nomine clara magis.
Nunc hæc, Argentoratum atque Colonia mœret
Orbas se tali (proh dolor )esseuiro.
Occidit ille quidem, at totus non occidit, eius
Pars licet hoc fragilis sit tumulata loco.
At melior pars æthereo gratissima cœlo
Diuorum consors semper honoris erit
Tumulus Ioanms DurhammeriTectus Ioannes hac Durhammerus arena.
Valde integer, doctus, probus, prudens, pius.
Qui quo crescentes magis accedebat ad annos,
Id quod fuit, fiebat indies magis.
Et tandem seipso melior maiorque futurus,
Si mortis improbitas pepercisset uiro.
Quæe licet abstulerit tot uirtutum organon ipsum.
Non, testimonium eius hoc, tamen, auferet.
Epitaphium defunctorum qui uno anno
Argentorati mor tui sunt.
Argentoratirus tria sepulchralia
Loca habet, Achiuis dicta cœmiteria,
Quorum unus Eurum, alius Zephiron & tertius
Boreamuidet, qui contegunt cadauera
Ter mille, bis centum, quaterq duo hominum,
Quos consulatu tunc ab inito Matthie
Gigæro, adusq nouissimum eiusdem diem
Conspexit annus unus omnes emori,
Quem funerum, at sexagies simulque ter
Dempta unitate, medietas numero refert.
Sic temporis breue spacium plus abstulit
Mortalium quàm longitudo protulit.
Heu nil, quod est corruptioni obnoxium,
Non destrui citius solet quam construi.
FINIS.
Argentorati in ædibus Vuendelini
Rihelii. Anno M. D. XLII.
Mense lunio.
