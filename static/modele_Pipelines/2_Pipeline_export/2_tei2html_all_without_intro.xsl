<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        exclude-result-prefixes="xs"
        version="2.0"
        xpath-default-namespace="http://www.tei-c.org/ns/1.0">
        
        <xsl:output method="html" encoding="UTF-8"/>
        <xsl:strip-space elements="*"/>
        
        <!-- Je modifie l'élément racine pour construire une page html -->
        
        <xsl:template match="/">
            <html>
                <head>
                    <title>Transcription du poème liminaire accompagnant l'<span rend="italic">Aphorismi institutionis puerorum</span> d'Otto Brunfels de Ioannes Sapidus.</title>
                </head>
                <body>
                    <style>
                        .verse {
                        display: flex;
                        justify-content: space-between;
                        }
                        
                        .verse-text {
                        margin-left: 20%;
                        }
                        
                        .verse-text-elegiac {
                        margin-left: 22%;
                        }
                        
                        .verse-number {
                        margin-right: 10%;
                        }
                 
                        .hover-note {
                        display: none;
                        position: absolute;
                        background-color: #fff;
                        border: 1px solid #ccc;
                        padding: 10px;
                        width: 200px;
                        font-size: 14px;
                        line-height: 1.5;
                        height: auto;
                        }
                        
                        sup:hover .hover-note {
                        display: inline-block;
                        }
                        
                        .tooltip {
                        position: relative;
                        display: inline-block;
                        color: blue;
                        font-weight: bold;
                        opacity: 1;
                        font-size: 14px;
                        }
                        
                        .tooltip .tooltip-content {
                        visibility: hidden;
                        width: 250px;
                        background-color: #f9f9f9;
                        color: #333;
                        text-align: center;
                        border: 1px solid #ccc;
                        border-radius: 4px;
                        padding: 5px;
                        position: absolute;
                        z-index: 9999;
                        bottom: 125%;
                        left: 50%;
                        transform: translateX(-50%);
                        opacity: 0;
                        transition: opacity 0.3s;
                        }
                        
                        .tooltip:hover .tooltip-content {
                        visibility: visible;
                        opacity: 1;
                        }
                    </style>
                    <xsl:apply-templates/>
                    <xsl:apply-templates select="//app"/>
                </body>
            </html>
        </xsl:template>
        

        <!-- Je ne copie pas le contenu de teiHeader and co -->
        
        <xsl:template match="teiHeader"/>
        <xsl:template match="orig"/>
        <xsl:template match="front"/>
        <xsl:template match="sourceDoc"/>
        <xsl:template match="lb"/>
        <xsl:template match="app"/>
        <xsl:template match="back"/>
        <xsl:template match="reg[@type='scansion_readable']"/>
        <xsl:template match="reg[@type='scansion_xml']"/>
        <xsl:template match="reg[@type='trad']"/>
        <xsl:template match="reg[@type='norm']"/>
        
       
        <xsl:template match="anchor">
            <xsl:choose>
                <xsl:when test="ends-with(@xml:id, 's')">
                    <xsl:text>&lt;span class="tooltip"></xsl:text>
                    <xsl:apply-templates/>
                </xsl:when>
                
                <xsl:when test="ends-with(@xml:id, 'e')">
                    <xsl:text>&lt;span class="tooltip-content"></xsl:text>
                    <xsl:variable name="lem" select="//app[@to=concat('#', current()/@xml:id)]/lem"/>
                    <xsl:variable name="rdgs" select="//app[@to=concat('#', current()/@xml:id)]/rdg"/>
                    
                    <!-- Affiche le contenu de <lem> et @wit de lem -->
                    <xsl:apply-templates select="$lem"/>
                    <xsl:if test="$lem/@wit">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="translate($lem/@wit, '#', '')"/>
                    </xsl:if>
                    
                    <xsl:text> : </xsl:text>
                    
                    <!-- Vérifie si le contenu de <lem> est identique à celui de <rdg1> -->
                    <xsl:if test="$lem = $rdgs[1]">
                        <xsl:value-of select="normalize-space($lem)"/>
                        <xsl:if test="$lem/@wit">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="translate($lem/@wit, '#', '')"/>
                        </xsl:if>
                        
                        <xsl:text> @wit lem @wit rdg1</xsl:text>
                    </xsl:if>
                    
                    <!-- Vérifie si le contenu de <lem> est identique à celui de <rdg2> -->
                    <xsl:if test="$lem = $rdgs[2]">
                        <xsl:if test="not($lem = $rdgs[1])">
                            <xsl:text> @wit lem @wit rdg2</xsl:text>
                        </xsl:if>
                    </xsl:if>
                    
                    <!-- Vérifie si le contenu de <rdg1> est identique à celui de <rdg2> -->
                    <xsl:if test="$rdgs[1] = $rdgs[2]">
                        <xsl:if test="not($lem = $rdgs[1])">
                            <xsl:text> @wit rdg1 @wit rdg2</xsl:text>
                        </xsl:if>
                    </xsl:if>
                    
                    <!-- Si aucun contenu n'est identique, affiche le contenu de <rdg1> et @wit de rdg1 -->
                    <xsl:if test="not($lem = $rdgs[1]) and not($rdgs[1] = $rdgs[2])">
                        <xsl:value-of select="normalize-space($rdgs[1])"/>
                        <xsl:if test="$rdgs[1]/@wit">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="translate($rdgs[1]/@wit, '#', '')"/>
                        </xsl:if>
                        
                        <xsl:text> : </xsl:text>
                    </xsl:if>
                    
                    <!-- Affiche le contenu de <rdg2> et @wit de rdg2 -->
                    <xsl:value-of select="normalize-space($rdgs[2])"/>
                    <xsl:if test="$rdgs[2]/@wit">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="translate($rdgs[2]/@wit, '#', '')"/>
                    </xsl:if>
                    
                    <xsl:text>&lt;/span>&lt;/span></xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy>
                        <xsl:apply-templates/>
                    </xsl:copy>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:template>

        <xsl:template match="p">
            <p style="text-align: justify;">
                <xsl:apply-templates/>
            </p>
        </xsl:template>
        
        <xsl:template match="head">
            <h2>
                <xsl:apply-templates/> 
            </h2>
        </xsl:template>
        
        <xsl:template match="ref[starts-with(@target, 'http')]">
            <a href="{@target}">
                <xsl:apply-templates/>
            </a>
        </xsl:template>
        
        <xsl:template match="span[@rend='italic']">
            <i>
                <xsl:apply-templates/>  
            </i>
        </xsl:template>
        
        <xsl:template match="hi">
             <h2>
                <xsl:apply-templates/>
             </h2>
        </xsl:template>
        
        <xsl:template match="span[@rend='bold']">
            <b>
                <xsl:apply-templates/>  
            </b>
        </xsl:template>
        
        <xsl:template match="list">
            <ul>
                <xsl:apply-templates/>
            </ul>
        </xsl:template>
       
        <xsl:template match="item">
            <li>
                <xsl:apply-templates/>
            </li>
        </xsl:template>
        
        <!-- Je mets en forme mes notes de bas de page avec renvois  -->
        
        <xsl:variable name="noteCount" select="count(//note)"/>
        
        <xsl:template match="note">
            <sup>
                <a name="note-{generate-id()}" href="#note-ref-{generate-id()}">
                    <xsl:number level="any" count="note"/>
                </a>
                <span class="hover-note">
                    <xsl:value-of select="."/>
                </span>
            </sup>
        </xsl:template>
        
        <xsl:template match="note" mode="footnote">
            <div class="footnote">
                <xsl:variable name="noteIndex">
                    <xsl:number level="any" count="note"/>
                </xsl:variable>
                <sup>
                    <a name="note-ref-{generate-id()}" href="#note-{generate-id()}">
                        <xsl:value-of select="$noteIndex"/>
                    </a>
                </sup>
                <xsl:value-of select="."/>
            </div>
        </xsl:template>


        <!-- Je traduis hi en h2 (h1 trop grand pour mon site) -->
        
        <xsl:template match="hi[@rend='Title']">
                    <h2>
                        <xsl:apply-templates/>
                    </h2>
          </xsl:template>

        <xsl:template match="l[not(@met='E')]">
            <div class="verse">
                <span class="verse-text"><xsl:apply-templates/></span>
                <xsl:variable name="verse-number" select="@n" />
                <xsl:if test="$verse-number mod 5 = 0">
                    <span class="verse-number"><xsl:value-of select="$verse-number" /></span>
                </xsl:if>
            </div>
        </xsl:template>
        
        
        <xsl:template match="l[@met='E']">
            <xsl:variable name="verse-number" select="@n" />
            <xsl:choose>
                <xsl:when test="number($verse-number) mod 2 = 0">
                    <div class="verse">
                        <span class="verse-text-elegiac"><xsl:apply-templates/></span>
                        <xsl:if test="$verse-number mod 5 = 0">
                            <span class="verse-number"><xsl:value-of select="$verse-number" /></span>
                        </xsl:if>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <div class="verse">
                        <span class="verse-text"><xsl:apply-templates/></span>
                        <xsl:if test="$verse-number mod 5 = 0">
                            <span class="verse-number"><xsl:value-of select="$verse-number" /></span>
                        </xsl:if>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:template>
        
        
 
        
        <!-- Je 'ajoute un break après le contenu de reg si dans l pour revenir à la ligne après les vers, sinon rien
             Je m'occupe aussi de réconcilier les mots et ajouter des espaces en réconciliant les lignes -->
        
        <xsl:template match="l/choice/reg[@type='ed']">
            <xsl:choose>
                <xsl:when test="contains(., '¬')">
                    <xsl:value-of select="translate(., '¬', '')"/>
                </xsl:when>
                <xsl:when test="contains(., 'non ulla lege')">
                    <xsl:value-of select="normalize-space(.)"/><xsl:text> </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:template>
        
        
        
        <!-- Je traduis ex en span, auquel j'ajoute des
        crochets avant et après -->
        
        <xsl:template match="ex">
            <span class="ex">
                <xsl:text>[</xsl:text>
                <xsl:apply-templates/>
                <xsl:text>]</xsl:text>
            </span>
        </xsl:template>
        
        <xsl:template match="div">
            <div><xsl:apply-templates/></div>
        </xsl:template>
        
        <xsl:template match="fw[@type='RunningTitleZone']">
            <p style='text-align : center;"'><xsl:apply-templates/></p>
        </xsl:template>
        
        <xsl:template match="fw[@type='QuireMarksZone']">
            <p style="text-align: right;"><xsl:apply-templates/></p>
        </xsl:template>
        
        <xsl:template match="fw[@type='NumberingZone']">
            <p style="text-align: left;"><xsl:apply-templates/></p>
        </xsl:template>
 
</xsl:stylesheet>