<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <xsl:output encoding="UTF-8" method="text" indent="no"/>

    <xsl:strip-space elements="*"/>
    <xsl:preserve-space elements="reg"/>

    <xsl:template match="teiHeader"/>
    <xsl:template match="sourceDoc"/>
    <xsl:template match="note"/>
    <xsl:template match="back"/>
    <xsl:template match="front"/>
    <xsl:template match="fw"/>
    <xsl:template match="orig"/>
    <xsl:template match="figure"/>
    <xsl:template match="reg[@type='norm']"/>
    <xsl:template match="reg[@type='scansion_readable']"/>
    <xsl:template match="reg[@type='scansion_xml']"/>


    <xsl:template match="reg[@type='ed']">
        <xsl:choose>
        <xsl:when test="contains(., '¬')">
            <xsl:value-of select="translate(normalize-space(.), '¬', '')"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="normalize-space(.)"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    

     
    <xsl:template match="hi[@rend = 'Title']">
        <xsl:for-each select="choice/reg[@type = 'ed']">
            <xsl:choose>
                <xsl:when test="position() = last()">
                    <xsl:value-of select="normalize-space(.)"/>
                    <xsl:text>&#xA;</xsl:text>
                </xsl:when>
                <xsl:when test="contains(., '¬')">
                    <xsl:value-of select="normalize-space(translate(., '¬', ''))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(normalize-space(.), ' ')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
   
</xsl:stylesheet>
