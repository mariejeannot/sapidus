<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei" xpath-default-namespace="">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:variable name="fichier2" select="document('input/scansion.xml')" />
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="tei:l">
        <xsl:copy>
            <xsl:copy-of select="@*" />
            
            <!-- Recherche de la correspondance entre name et n -->
            <xsl:variable name="matching-line" select="$fichier2//line[@name = current()/@n]" />
            
            <!-- Ajout des attributs met et real -->
            <xsl:attribute name="met">
                <xsl:value-of select="$matching-line/@meter"/>
            </xsl:attribute>
            
            <xsl:attribute name="real">
                <xsl:value-of select="$matching-line/@pattern" />
            </xsl:attribute>
            
            <xsl:apply-templates select="node()" />
        </xsl:copy>
    </xsl:template>
    
    <!-- Pour mettre aux normes de la TEI les balises word -->
    
    <xsl:template match="tei:word">
        <xsl:element name="w" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
    </xsl:template>
    
</xsl:stylesheet>
