<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0">
    
    <!-- On part d'abord du notebook Scansion_aide pour obtenir un xml de scansion_readable -->
    <!-- Attention : mettre scansion.xml dans un fichier tei pour bien convertir -->
    
    <xsl:variable name="fichier2" select="document('output/scansion_readable.xml')"/>
    <xsl:output method="xml" indent="yes"/>
        
        <!-- Modèle principal pour le traitement du fichier texte -->
        <xsl:template match="/">
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
                <teiHeader>
                    <fileDesc>
                        <titleStmt>
                            <title>Title</title>
                        </titleStmt>
                        <publicationStmt>
                            <p>Publication Information</p>
                        </publicationStmt>
                        <sourceDesc>
                            <p>Information about the source</p>
                        </sourceDesc>
                    </fileDesc>
                </teiHeader>
                <text>
                    <body>
                        <xsl:variable name="lines" select="tokenize(unparsed-text('output/scansion_readable.txt'), '\r?\n')"/>
                        <xsl:for-each select="$lines">
                            <l n="{position()}">
                                <xsl:value-of select="."/>
                            </l>
                        </xsl:for-each>
                    </body>
                </text>
            </TEI>
        </xsl:template>
        
    </xsl:stylesheet>
    