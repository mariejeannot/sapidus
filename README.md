![](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)  
![](https://img.shields.io/badge/license-AGPL3.0-informational?logo=gnu&color=important)


Le site web https://mariejeannot.gitpages.huma-num.fr/sapidus/ a été construit grâce à [Hugo](https://gohugo.io) et à [GitLab Pages](https://about.gitlab.com/features/pages/).


La documentation officielle d'Hugo se trouve [à cette adresse](https://gohugo.io/documentation/).
Vous pouvez en apprendre davantage sur les GitLab Pages [sur cette page](https://pages.gitlab.io) ; la documentation officielle se trouve [à cette adresse](https://docs.gitlab.com/ce/user/project/pages/).

---

### Qui suis-je ?

Moi, c'est [Marie Jeannot-Tirole](https://orcid.org/0000-0002-8064-0977). 
Mes travaux de thèse portent sur la _Sylua epistolaris seu Barba_ de Sapidus.
 
Mon objectif est de partager toutes mes données liées à Sapidus.
