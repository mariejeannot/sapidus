## Bienvenue !
      
⚠ Le site est encore en construction et sera alimenté au fur et à mesure de mes avancées dans les mois et années à venir. ⚠

À terme, vous trouverez sur ce site diverses informations concernant le poète sélestadien **Ioannes Sapidus (1490-1561)** :
1. Correspondance de Sapidus
     - [Liste chronologique de la correspondance de Sapidus](page/cor/list) (pas encore disponible)
     - [Transcription et traduction de sa correspondance](page/cor/transcription) (pas encore disponible)
2. Œuvres de Sapidus
     - [Liste chronologique des œuvres de Sapidus](page/po/list) (en cours d'élaboration)
     - [_Ode de lege et euangelio_](page/po/works/ode) (édition)
     - [_Sylua epistolaris seu Barba_](page/po/works/apologia) (édition de l'intégralité de l'imprimé)
     - [_Apotheosis Erasmi_ et _De poculo_](page/po/works/apotheosis) (transcriptions)
3. Biographie de Sapidus
     - [Frise chronologique de la vie de Sapidus](page/bio/frise) (pas encore disponible)
     - [Transcription et traduction d'archives concernant Sapidus](page/bio/transcription) (pas encore disponible)
4. Documents autographes de Sapidus
     - [Liste des documents autographes de Sapidus](page/auto/list) (pas encore disponible)
