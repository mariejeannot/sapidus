---
title: Marie Jeannot-Tirole
subtitle: doctrorante contractuelle en Philologie classique à l'Université de Strasbourg
---

## Formations universitaires
  
#### **(2021-en cours)** Doctorat en philologique classique **(Université de Strasbourg)**

- « Un poème humaniste né de l’héritage antique : la _Sylua epistolaris seu Barba_ de Ioannes Sapidus (1490-1561) », dir. James Hirstein, maître de conférences en Langue et littérature latines (Université de Strasbourg). [Projet de recherche, mai 2021.](https://git.unistra.fr/marie.jeannot/sapidus/-/blob/master/static/Projet_de_recherche_Marie_Jeannot-Tirole.pdf)
- Contrat doctoral au sein de l'Université de Strasbourg d'octobre 2021 à septembre 2024.

#### **(2022-2023)** Certificat de sprécialisation en Humanités numériques **(Université de Genève)**

- Diplôme complémentaire proposé par l'Université de Genève.
- « Introduction aux Humanités numériques » (Béatrice Joyeux-Prunel) ; « Distant Reading » et « Numériser le patrimoine » (Simon Gabay).

#### **(2021)** Agrégation de Lettres Classiques

#### **(2018-2020)** Master de philologique classique **(Université de Strasbourg)**

- _Érasme, traducteur de l’_ Hécube _d’Euripide_, dir. Johann Goeken et James Hirstein.
- Soutenu le 24 juin 2020 à l’Université de Strasbourg.

#### **(2015-2018)** Licence de Lettres Classiques **(Université de Strasbourg)**

#### **(2015-2018)** Licence de Langue, littérature et civilisation étrangère, parcours japonais **(Université de Strasbourg)**

---

## Autres formations

#### **(2022)** Stage d'ecdotique des Sources chrétiennes

- Paléographie latine.
- Ecdotique.
- Introduction aux Humanités numériques.

#### **(2019)** Académie des langues anciennes, grec confirmé  

---

## Travaux scientifiques publiés

- « Érasme et Anselmo, traducteurs ou poètes ? », colloque "Pourquoi traduire ? », Strasbourg (14-16 novembre 2022), publication à venir.
- « L'Hécube d'Érasme, bien plus qu'une traduction ? », congrès SEMEN-L, Dijon (8-11 juin 2022), article à paraître dans la revue _Camena_.
- HIRSTEIN James et JEANNOT Marie, « Beatus Rhenanus et Johann Sapidus en 1543 : les épitaphes des comtes de Wertheim et la Réforme », in _Annuaire des Amis de la Bibliothèque Humaniste de Sélestat_ (AABHS - 70), 2020, pp. 165-186.
- _Érasme, traducteur de l’Hécube d’Euripide_, mémoire de master sous la direction de Johann Goeken et James Hirstein, soutenu le 24 juin 2020 à l’université de Strasbourg.
- HIRSTEIN James et JEANNOT Marie, « Kaspar Stiblin (1526-1562) et la bibliothèque de Beatus Rhenanus : Euripide et Cicéron », in _AABHS_ (69), 2019, pp. 57-70.

---

## Humanités numériques

- _Data repository for HTR model training_, Université de Strasbourg et Université de Genève, Genève, 2023. https://github.com/Johann-Sapidus-1490-1561/HTR
- Aide à la transcription d'archives du XVIe siècle.
    1. Peutinger, Konrad: Brief an Johannes Sapidus. Augsburg, 8. Jan. 1523. Universitätsbibliothek Basel, UBH G2 I 37, 14 https://doi.org/10.7891/e-manuscripta-99274 / Public Domain Mark


---

## Projets en cours

- Édition-traduction-commentaire des *Epigrammata* de Ioannes Sapidus en collaboration avec James Hirstein et Marie-Odile Burckel.

---
  
    
   
##### En savoir plus
  
N'hésitez pas à me contacter pour échanger à propos de **Ioannes Sapidus** (1490-1561) à l'adresse suivante : marie.jeannot@unistra.fr ou à consulter mon [CV](https://git.unistra.fr/marie.jeannot/sapidus/-/blob/master/static/CV_Marie_Jeannot-Tirole.pdf).
