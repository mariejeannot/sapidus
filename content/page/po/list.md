---
title: La poésie de Sapidus
date: 2022-10-04
---

### Liste des oeuvres poétiques de Sapidus par ordre chronologique

#### Poèmes sans date

- [_Ode de lege et euangelio_](../works/ode), s. d, vers 1520? (édition).

#### Poèmes datées par année de publication
- [_Consolatorium ad Iacobum Vuinphelingum_](../works/consolatorium), 1514? (transcription).
- [_Ad Sodales Erasmo Roterodamo consuetudine iunctissimos_](../works/ad_sodales), 1515/1518 (transcription).
- [_Qua ratione puer primus formetur ab annis_](../works/aphorismi), 1519 (transcription).
- [_Liber loquitur_](../works/liber_loquitur), 1520 (transcription).
- Recueil des [_Epigrammata_](../works/epigrammata), 1520 (édition).
- [_Sylua epistolaris seu Barba_ (avec le _Pro sacerdotum barbis_ de Valeriano)](../works/apologia), 1526/1534 (édition).
- [_De cometa_](../works/apologia), 1534 (transcription).
- [_Metallici iam morientis, & Diuitiarum eiusdem colloquium_](../works/apologia), 1534 (transcription).
- [_Apotheosis Erasmi_](../works/apotheosis), 1536/1537 (transcription).
- [_De poculo_](../works/apotheosis), 1536/1537 (transcription).

#### Nota Bene

Si plusieurs poèmes ont été édités dans un même volume imprimé, nous faisons des entrées distinctes dans notre liste, mais nous renvoyons à la page HTML appelée du nom du poème majeur.
**Exemple** : le _De poculo_ a été publié avec l'_Apotheosis Erasmi_. Le lien renvoie donc vers la page "works/apotheosis/". Les liens de téléchargement et les fichiers du _De poculo_ seront également appelés "Apotheosis.xml/pdf/txt". 

La même [norme de transcription](../transcription) a été utilisée (sauf mention contraire détaillée avant le poème).

Le titre des poèmes correspond à la version abrégée du titre indiqué sur l'imprimé ou à défaut de titre au premier vers du poème. Pour retrouver les références exactes d'un poème, se reporter à la [liste détaillée des œuvres de Sapidus](../static/Liste_oeuvres).

