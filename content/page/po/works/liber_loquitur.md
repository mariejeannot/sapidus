---
date: 2023-11-10
---

# <center>Transcription du poème liminaire de Ioannes Sapidus qui accompagne le commentaire de l'_Epître aux Galates_ de Martin Luther ([Strasbourg/Bâle], 1520)</center>


<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Liber_loquitur_1520/Liber_loquitur_1520.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Liber_loquitur_1520/Liber_loquitur_1520.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Liber_loquitur_1520/Liber_loquitur_1520.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Liber_loquitur_1520/Liber_loquitur_1520.html" >}}