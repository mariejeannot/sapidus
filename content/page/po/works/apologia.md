---
date: 2023-05-31
---

# <center>Édition numérique du _Pro Sacerdotum Barbis_ et de la _Sylua epistolaris seu Barba_ (_Apologia_, Strasbourg, 1534)</center>

<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Sylua_1534/Apologia_1534.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Sylua_1534/Apologia_1534.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Sylua_1534/Apologia_1534.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>
  
   <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Sylua_1534/Apologia_1534.pdf?inline=false" class="btn" download>
    <button class="button">Format PDF</button>
  </a>
  
   <a href="https://mariejeannot.pages.unistra.fr/sapidus/page/po/works/apodd/" class="btn">
    <button class="button">ODD</button>
  </a>
 
   <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Sylua_1534/Projet_edition_numerique_de_l_Apologia.pdf?inline=false" class="btn" download>
    <button class="button">Descriptif du projet</button>
  </a>

  {{< readHTML "../../static/po/Sylua_1534/Apologia_1534.html" >}}
  


