---
date: 2023-05-31
---

# <center>Transcriptions de l'_Apotheosis Erasmi_ et du _De poculo_ de Ioannes Sapidus (1536)</center>

<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Apotheosis_1536/Apotheosis_1536.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Apotheosis_1536/Apotheosis_1536.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Apotheosis_1536/Apotheosis_1536.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Apotheosis_1536/Apotheosis_1536.html" >}}
  


