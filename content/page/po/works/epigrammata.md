---
date: 2023-11-07
---

# <center>Édition numérique des _Epigrammata_ de Ioannes Sapidus (Sélestat, 1520)</center>

<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Epigrammata_1520/Epigrammata_1520.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Epigrammata_1520/Epigrammata_1520.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Epigrammata_1520/Epigrammata_1520.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Epigrammata_1520/Epigrammata_1520.html" >}}