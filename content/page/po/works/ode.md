---
date: 2023-05-31
---

# <center>Édition numérique de l'_Ode sur la loi et l'évangile_ de Ioannes Sapidus</center>

<a href="https://git.unistra.fr/marie.jeannot/sapidus/-/raw/master/static/Ode/Ode.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/marie.jeannot/sapidus/-/blob/master/static/Ode/Ode.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/marie.jeannot/sapidus/-/blob/master/static/Ode/Ode.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Ode_sd/Ode.html" >}}
  


