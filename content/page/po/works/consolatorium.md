---
date: 2023-11-07
---

# <center>Transcription du _Consolatorium ad Iacobum Vuinphelingum_ de Ioannes Sapidus (Strasbourg?, 1514?)</center>

<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Consolatorium_1514/Consolatorium.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Consolatorium_1514/Consolatorium.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Consolatorium_1514/Consolatorium.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Consolatorium_1514/Consolatorium_1514.html" >}}