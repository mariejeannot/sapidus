---
date: 2023-11-07
---

# <center>Transcription du poème liminaire de Ioannes Sapidus qui accompagne l'_Aphorismi institutionis puerorum_ d'Otto Brunfels (Strasbourg, 1519)</center>


<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Aphorismi_1519/Aphorismi_1519.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Aphorismi_1519/Aphorismi_1519.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Aphorismi_1519/Aphorismi_1519.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Aphorismi_1519/Aphorismi_1519.html" >}}