---
date: 2023-11-10
---

# <center>Transcription du _Ad Sodales Erasmo Roterodamo consuetudine iunctissimos_ de Ioannes Sapidus (Bâle, 1518)</center>


<a href="https://git.unistra.fr/mariejeannot/sapidus/-/raw/master/static/Ad_Sodales_1515/Ad_Sodales_1515.html?inline=false" class="btn" download>
    <button class="button">Format HTML</button>
  </a>

  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Ad_Sodales_1515/Ad_Sodales_1515.xml?inline=false" class="btn" download>
    <button class="button">Format XML-TEI</button>
  </a>
  
  <a href="https://git.unistra.fr/mariejeannot/sapidus/-/blob/master/static/Ad_Sodales_1515/Ad_Sodales_1515.txt?inline=false" class="btn" download>
    <button class="button">Format TXT</button>
  </a>

  {{< readHTML "../../static/po/Ad_Sodales_1515/Ad_Sodales_1515.html" >}}